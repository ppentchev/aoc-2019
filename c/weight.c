/**
 * Copyright (c) 2019  Peter Pentchev <roam@ringlet.net>
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE AUTHOR AND CONTRIBUTORS ``AS IS'' AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED.  IN NO EVENT SHALL THE AUTHOR OR CONTRIBUTORS BE LIABLE
 * FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
 * DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS
 * OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
 * HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
 * LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY
 * OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF
 * SUCH DAMAGE.
 */

#include <err.h>
#include <inttypes.h>
#include <stdbool.h>
#include <stdio.h>
#include <string.h>

#include "rocket.h"
#include "weight.h"

static uint64_t calc_fuel(const uint64_t weight)
{
	const uint64_t third = weight / 3;
	if (third < 2)
		return 0;
	return third - 2;
}

static uint64_t calc_fuel_recursive(const uint64_t weight)
{
	uint64_t current = weight;
	uint64_t total = 0;
	while (true) {
		const uint64_t fuel = calc_fuel(current);
		if (fuel == 0)
			return total;
		total += fuel;
		current = fuel;
	}
}

static int run_weight(const char * const fname,
	uint64_t (*calc)(uint64_t))
{
	FILE * const fp = fopen(fname, "rt");
	if (fp == NULL)
		err(1, "Could not open %s", fname);

	char *line = NULL;
	size_t n = 0;
	uint64_t total = 0;
	while (getline(&line, &n, fp) != -1) {
		size_t len = strlen(line);
		while (len > 0 &&
		    (line[len - 1] == '\r' || line[len - 1] == '\n'))
			line[--len] = '\0';

		const uint64_t weight = strtouint64_fatal(line);
		const uint64_t fuel = calc(weight);
		total += fuel;
		printf("%ju %ju %ju\n",
		    (uintmax_t)weight, (uintmax_t)fuel, (uintmax_t)total);
	}
	if (ferror(fp))
		err(1, "Could not read from %s", fname);

	printf("%ju\n", (uintmax_t)total);
	return 0;
}

int run_weight_naive(const int argc, char * const argv[])
{
	if (argc != 2)
		errx(1, "A single filename expected");

	return run_weight(argv[1], calc_fuel);
}

int run_weight_recursive(const int argc, char * const argv[])
{
	if (argc != 2)
		errx(1, "A single filename expected");

	return run_weight(argv[1], calc_fuel_recursive);
}
