/*-
 * Copyright (c) 2019  Peter Pentchev
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE AUTHOR AND CONTRIBUTORS ``AS IS'' AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED.  IN NO EVENT SHALL THE AUTHOR OR CONTRIBUTORS BE LIABLE
 * FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
 * DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS
 * OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
 * HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
 * LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY
 * OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF
 * SUCH DAMAGE.
 */

#include <sys/types.h>
#include <sys/stat.h>
#include <sys/wait.h>

#include <err.h>
#include <errno.h>
#include <inttypes.h>
#include <limits.h>
#include <stdarg.h>
#include <stdbool.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>

#define ROCKET_NEED_DEBUG

#include "rocket.h"
#include "intcode.h"
#include "weight.h"

#define VERSION_STRING	"0.1.0"

static bool		verbose;

uint64_t strtouint64_fatal(const char * const line)
{
	if (line[0] == '\0')
		errx(1, "Empty integer value");

	errno = 0;
	char *end;
	const uint64_t res = strtoull(line, &end, 10);
	if (res == ULLONG_MAX && errno != 0)
		err(1, "Invalid integer value '%s'", line);
	if (*end != '\0')
		errx(1, "Invalid integer value '%s'", line);

	return res;
}

int64_t strtoint64_fatal(const char * const line)
{
	if (line[0] == '\0')
		errx(1, "Empty integer value");

	errno = 0;
	char *end;
	const int64_t res = strtoll(line, &end, 10);
	if ((res == LLONG_MIN || res == LLONG_MAX) && errno != 0)
		err(1, "Invalid integer value '%s'", line);
	if (*end != '\0')
		errx(1, "Invalid integer value '%s'", line);

	return res;
}

static void
usage(const bool _ferr)
{
	const char * const s =
	    "Usage:\taoc 01-1 weight-list.txt\n"
	    "\taoc-01-2 weight-list.txt\n"
	    "\taoc-02-1 program.txt\n"
	    "\taoc implemented\n"
	    "\taoc -V | -h | --version | --help\n"
	    "\taoc --features\n"
	    "\n"
	    "\t-h\tdisplay program usage information and exit\n"
	    "\t-V\tdisplay program version information and exit\n"
	    "\t-v\tverbose operation; display diagnostic output\n";

	fprintf(_ferr? stderr: stdout, "%s", s);
	if (_ferr)
		exit(1);
}

static void
version(void)
{
	puts("aoc " VERSION_STRING);
}

static void
features(void)
{
	puts("Features: aoc=" VERSION_STRING);
}

void
debug(const char * const fmt, ...)
{
	va_list v;

	va_start(v, fmt);
	if (verbose)
		vfprintf(stderr, fmt, v);
	va_end(v);
}

static int list_implemented(const int argc, char * const argv[]);

static struct cmd_t {
	const char * const name;
	int (*func)(int argc, char * const argv[]);
} commands[] = {
	{"implemented", list_implemented},
	{"01-1", run_weight_naive},
	{"01-2", run_weight_recursive},
	{"02-1", run_intcode_run},
	{"02-2", run_intcode_seek},
	{"05-1", run_intcode_diag},
	{"05-2", run_intcode_diag},
};
#define COMMANDS_COUNT	(sizeof(commands) / sizeof(commands[0]))

static int list_implemented(const int argc, char * const argv[] __unused)
{
	if (argc != 1)
		errx(1, "No arguments to 'implemented'");

	for (size_t idx = 0; idx < COMMANDS_COUNT; idx++) {
		const char * const name = commands[idx].name;
		if (name[0] >= '0' && name[0] <= '9')
			puts(name);
	}
	return 0;
}

int
main(int argc, char * const argv[])
{
	bool hflag = false, Vflag = false, show_features = false;
	int ch;
	while (ch = getopt(argc, argv, "hVv-:"), ch != -1)
		switch (ch) {
			case 'h':
				hflag = true;
				break;

			case 'V':
				Vflag = true;
				break;

			case 'v':
				verbose = true;
				break;

			case '-':
				if (strcmp(optarg, "help") == 0)
					hflag = true;
				else if (strcmp(optarg, "version") == 0)
					Vflag = true;
				else if (strcmp(optarg, "features") == 0)
					show_features = true;
				else {
					warnx("Invalid long option '%s' specified", optarg);
					usage(true);
				}
				break;

			default:
				usage(1);
				/* NOTREACHED */
		}
	if (Vflag)
		version();
	if (hflag)
		usage(false);
	if (show_features)
		features();
	if (Vflag || hflag || show_features)
		return (0);

	argc -= optind;
	argv += optind;
	if (argc < 1)
		usage(true);

	const char * const name = argv[0];
	debug("Looking for the '%s' command\n", name);
	for (size_t idx = 0; idx < COMMANDS_COUNT; idx++)
		if (strcmp(name, commands[idx].name) == 0)
			return commands[idx].func(argc, argv);
	errx(1, "Unknown command '%s'", name);
}
