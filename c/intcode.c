/**
 * Copyright (c) 2019  Peter Pentchev <roam@ringlet.net>
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE AUTHOR AND CONTRIBUTORS ``AS IS'' AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED.  IN NO EVENT SHALL THE AUTHOR OR CONTRIBUTORS BE LIABLE
 * FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
 * DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS
 * OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
 * HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
 * LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY
 * OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF
 * SUCH DAMAGE.
 */

#include <err.h>
#include <inttypes.h>
#include <stdbool.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#define ROCKET_NEED_DEBUG

#include "rocket.h"
#include "intcode.h"

enum opcode_t {
	OP_ADD = 1,
	OP_MUL = 2,

	OP_INPUT = 3,
	OP_OUTPUT = 4,

	OP_JUMP_IF_TRUE = 5,
	OP_JUMP_IF_FALSE = 6,
	OP_LESS_THAN = 7,
	OP_EQUALS = 8,

	OP_STOP = 99,
};

enum param_mode_t {
	MODE_POS = 0,
	MODE_IMMEDIATE = 1,
};

struct testdata {
	int replace;
	int64_t peek;
	int64_t value;
	int64_t seek;

	size_t code_len;
	int64_t *code;

	size_t memory_len;
	int64_t *memory;

	size_t input_len, input_pos;
	int64_t *input;

	int64_t current;
};

static void
append_values(int64_t ** const ptr, size_t * const len_ptr,
		char * const line)
{
	// Count the data (code or input) points...
	size_t count = 1;
	char *comma = line;
	while (comma = strchr(comma, ','), comma != NULL) {
		count++;
		comma++;
	}

	int64_t * const ncode = realloc(*ptr,
	    (*len_ptr + count) * sizeof(**ptr));
	if (ncode == NULL)
		err(1, "Out of memory");
	*ptr = ncode;

	char *field = line;
	for (size_t idx = 0; idx < count; idx++) {
		comma = strchr(field, ',');
		if (comma != NULL)
			*comma = '\0';
		(*ptr)[(*len_ptr)++] =
		    strtoint64_fatal(field);
		field = comma + 1;
	}
}

static void
load_file(const char * const fname, struct testdata * const data)
{
	data->replace = -1;
	data->peek = data->value = data->seek = 0;
	data->code_len = 0;
	data->code = NULL;
	data->memory_len = 0;
	data->memory = NULL;
	data->input_len = data->input_pos = 0;
	data->input = NULL;

	FILE * const fp = fopen(fname, "r");
	if (fp == NULL)
		err(1, "Could not open %s", fname);

	char *line = NULL;
	size_t length = 0;
	while (getline(&line, &length, fp) != -1) {
		size_t llen = strlen(line);
		while (llen > 0 &&
		    (line[llen - 1] == '\r' || line[llen - 1] == '\n'))
			line[--llen] = '\0';

		char * const eq = strchr(line, '=');
		if (eq != NULL) {
			*eq = '\0';
			if (strcmp(line, "replace") == 0)
				data->replace = strtouint64_fatal(eq + 1);
			else if (strcmp(line, "peek") == 0)
				data->peek = strtouint64_fatal(eq + 1);
			else if (strcmp(line, "value") == 0)
				data->value = strtouint64_fatal(eq + 1);
			else if (strcmp(line, "seek") == 0)
				data->seek = strtouint64_fatal(eq + 1);
			else if (strcmp(line, "input") == 0)
				append_values(&data->input, &data->input_len, eq + 1);
			else
				errx(1, "FIXME: unknown variable %s", line);
		} else {
			append_values(&data->code, &data->code_len, line);
		}
	}
	if (ferror(fp))
		err(1, "Could not read from %s", fname);

	fclose(fp);
}

static void
set_value(struct testdata * const data, const int64_t offset,
	const int64_t value)
{
	if (offset < 0)
		errx(1, "Invalid negative offset %jd", (intmax_t)offset);
	const size_t uoffset = (size_t)offset;
	if (uoffset >= data->memory_len) {
		int64_t *nmem = realloc(data->memory,
		    (uoffset + 1) * sizeof(*data->memory));
		if (nmem == NULL)
			err(1, "Out of memory");
		data->memory = nmem;

		for (size_t idx = data->memory_len; idx <= uoffset; idx++)
			data->memory[idx] = 0;
		data->memory_len = uoffset + 1;
	}

	data->memory[uoffset] = value;
}

static int64_t
get_value(const struct testdata * const data, const int64_t offset)
{
	if (offset < 0)
		errx(1, "Invalid negative offset %jd", (intmax_t)offset);
	const size_t uoffset = (size_t)offset;
	if (uoffset >= data->memory_len)
		return 0;
	return data->memory[uoffset];
}

static void
init_memory(struct testdata * const data)
{
	data->memory_len = data->code_len;
	data->memory = realloc(data->memory,
	    data->memory_len * sizeof(*data->memory));
	memcpy(data->memory, data->code, data->code_len * sizeof(*data->code));

	data->current = 0;

	if (data->replace != -1) {
		unsigned noun = data->replace / 100, verb = data->replace % 100;
		set_value(data, 1, noun);
		set_value(data, 2, verb);
	}
}

static int64_t
get_p_value(const struct testdata * const data, const enum param_mode_t mode, const int64_t param)
{
	switch (mode) {
		case MODE_POS:
			return get_value(data, param);

		case MODE_IMMEDIATE:
			return param;

		default:
			errx(1, "Invalid parameter mode %d\n", mode);
			/* NOTREACHED */
	}
	/* NOTREACHED */
}

static int64_t
pop_input_value(struct testdata * const data)
{
	if (data->input_pos == data->input_len)
		errx(1, "No data for input");
	data->input_pos++;
	return data->input[data->input_pos - 1];
}

static void
push_output_value(const struct testdata * const data __unused,
		const int value)
{
	printf("OUTPUT %jd\n", (intmax_t)value);
}

static void
do_arith(struct testdata * const data, const int opcode_val)
{
	const enum opcode_t opcode = opcode_val % 100;
	const enum param_mode_t mode_1 = (opcode_val / 100) % 10;
	const enum param_mode_t mode_2 = (opcode_val / 1000) % 10;
	if (opcode_val > 10000)
		errx(1, "Too many parameter modes for arithmetics");

	const int64_t offset_1 = get_value(data, data->current + 1);
	const int64_t offset_2 = get_value(data, data->current + 2);
	const int64_t store = get_value(data, data->current + 3);

	const int64_t op1 = get_p_value(data, mode_1, offset_1);
	const int64_t op2 = get_p_value(data, mode_2, offset_2);

	int64_t result;
	switch (opcode) {
		case OP_ADD:
			result = op1 + op2;
			break;

		case OP_MUL:
			result = op1 * op2;
			break;

		default:
			errx(1, "Internal error: opcode %ju in do_arith()",
			    (intmax_t)opcode);
			/* NOTREACHED */
	}
	debug("do_arith: op1 %jd op2 %jd result %jd store %jd\n", (intmax_t)op1, (intmax_t)op2, (intmax_t)result, (intmax_t)store);
	set_value(data, store, result);

	data->current += 4;
}

static void
do_input(struct testdata * const data, const int opcode_val)
{
	if (opcode_val > 100)
		errx(1, "Too many parameter modes for input");

	const int64_t store = get_value(data, data->current + 1);
	debug("do_input: about to fetch a value for %jd\n",
	    (intmax_t)store);
	const int64_t value = pop_input_value(data);
	debug("do_input: about to store %jd into %jd\n",
	    (intmax_t)value, (intmax_t)store);
	set_value(data, store, value);

	data->current += 2;
}

static void
do_output(struct testdata * const data, const int opcode_val)
{
	const enum param_mode_t mode_1 = (opcode_val / 100) % 10;
	if (opcode_val > 1000)
		errx(1, "Too many parameter modes for output");

	const int64_t offset_1 = get_value(data, data->current + 1);

	const int64_t value = get_p_value(data, mode_1, offset_1);
	debug("do_output: about to output %jd (arg %jd mode %d)\n",
	    (intmax_t)value, (intmax_t)offset_1, mode_1);
	push_output_value(data, value);

	data->current += 2;
}

static void
do_compare(struct testdata * const data, const int opcode_val)
{
	const enum opcode_t opcode = opcode_val % 100;
	const enum param_mode_t mode_1 = (opcode_val / 100) % 10;
	const enum param_mode_t mode_2 = (opcode_val / 1000) % 100;
	if (opcode_val > 10000)
		errx(1, "Too many parameter modes for comparison");

	const int64_t offset_1 = get_value(data, data->current + 1);
	const int64_t offset_2 = get_value(data, data->current + 2);
	const int64_t store = get_value(data, data->current + 3);

	const int64_t op1 = get_p_value(data, mode_1, offset_1);
	const int64_t op2 = get_p_value(data, mode_2, offset_2);

	int64_t result;
	switch (opcode) {
		case OP_LESS_THAN:
			result = op1 < op2;
			break;

		case OP_EQUALS:
			result = op1 == op2;
			break;

		default:
			errx(1, "Internal error: comparison opcode %d",
			    opcode);
			/* NOTREACHED */
	}
	debug("do_compare: op1 %jd op2 %jd result %jd store %jd\n", (intmax_t)op1, (intmax_t)op2, (intmax_t)result, (intmax_t)store);
	set_value(data, store, result);

	data->current += 4;
}

static void
do_jump_if(struct testdata * const data, const int opcode_val)
{
	const enum opcode_t opcode = opcode_val % 100;
	const enum param_mode_t mode_1 = (opcode_val / 100) % 10;
	const enum param_mode_t mode_2 = (opcode_val / 1000) % 100;
	if (opcode_val > 10000)
		errx(1, "Too many parameter modes for conditional jump");

	const int64_t offset_1 = get_value(data, data->current + 1);
	const int64_t offset_2 = get_value(data, data->current + 2);

	const int64_t test = get_p_value(data, mode_1, offset_1);
	const int64_t target = get_p_value(data, mode_2, offset_2);

	int64_t result;
	switch (opcode) {
		case OP_JUMP_IF_TRUE:
			result = test;
			break;

		case OP_JUMP_IF_FALSE:
			result = !test;
			break;

		default:
			errx(1, "Internal error: comparison opcode %d",
			    opcode);
			/* NOTREACHED */
	}
	debug("do_jump_if: test %jd target %jd result %jd\n", (intmax_t)test, (intmax_t)target, (intmax_t)result);
	if (result)
		data->current = target;
	else
		data->current += 3;
}

static void
run(struct testdata * const data)
{
	while (true) {
		const int64_t opcode = get_value(data, data->current);
		debug("run: %16jd: opcode %jd\n",
		    (intmax_t)data->current, (intmax_t)opcode);
		switch (opcode % 100) {
			case OP_STOP:
				return;

			case OP_ADD:
			case OP_MUL:
				do_arith(data, opcode);
				break;

			case OP_INPUT:
				do_input(data, opcode);
				break;

			case OP_OUTPUT:
				do_output(data, opcode);
				break;

			case OP_JUMP_IF_TRUE:
			case OP_JUMP_IF_FALSE:
				do_jump_if(data, opcode);
				break;

			case OP_LESS_THAN:
			case OP_EQUALS:
				do_compare(data, opcode);
				break;

			default:
				errx(1, "Invalid opcode %ju",
				    (intmax_t)opcode);
				/* NOTREACHED */
		}
	}
}

int
run_intcode_run(const int argc, char * const argv[])
{
	if (argc != 2)
		errx(1, "A single program filename expected");

	struct testdata data = { 0 };
	load_file(argv[1], &data);
	init_memory(&data);
	run(&data);

	printf("%ju %ju\n",
	    (intmax_t)data.peek,
	    (intmax_t)get_value(&data, data.peek));
	return 0;
}

int
run_intcode_seek(const int argc, char * const argv[])
{
	if (argc != 2)
		errx(1, "A single program filename expected");

	struct testdata data = { 0 };
	load_file(argv[1], &data);

	for (size_t noun = 0; noun < data.code_len + 1; noun++)
		for (size_t verb = 0; verb < data.code_len + 1; verb++) {
			data.replace = noun * 100 + verb;
			init_memory(&data);
			run(&data);
			
			const int64_t value = get_value(&data, data.peek);
			if (value == data.seek) {
				printf("%d\n", data.replace);
				return 0;
			}
		}

	puts("-1");
	return 0;
}

int
run_intcode_diag(const int argc, char * const argv[])
{
	if (argc != 2)
		errx(1, "A single program filename expected");

	struct testdata data = { 0 };
	load_file(argv[1], &data);
	init_memory(&data);
	run(&data);

	/* No extraneous output in diagnostic mode. */
	return 0;
}
