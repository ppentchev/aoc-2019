# Copyright (c) 2019  Peter Pentchev <roam@ringlet.net>
# All rights reserved.
#
# Redistribution and use in source and binary forms, with or without
# modification, are permitted provided that the following conditions
# are met:
# 1. Redistributions of source code must retain the above copyright
#    notice, this list of conditions and the following disclaimer.
# 2. Redistributions in binary form must reproduce the above copyright
#    notice, this list of conditions and the following disclaimer in the
#    documentation and/or other materials provided with the distribution.
#
# THIS SOFTWARE IS PROVIDED BY THE AUTHOR AND CONTRIBUTORS ``AS IS'' AND
# ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
# IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
# ARE DISCLAIMED.  IN NO EVENT SHALL THE AUTHOR OR CONTRIBUTORS BE LIABLE
# FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
# DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS
# OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
# HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
# LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY
# OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF
# SUCH DAMAGE.
""" Test the weight calculations. """

import argparse
import enum
import json
import pathlib
import subprocess

from typing import Callable, List, NamedTuple


class WeightMode(enum.IntEnum):
    """ Which weight calculation to run? """

    NAIVE = 0
    RECURSIVE = 1


class Config(NamedTuple):
    """ Configuration information for the creation of files. """

    datadir: pathlib.Path
    play: pathlib.Path
    program: List[str]


class TestData(NamedTuple):
    """ A single module's weight data. """

    weight: int
    naive: int
    recursive: int


def read_test_data(cfg: Config) -> List[TestData]:
    """ Create the test file for the weights calculations. """
    data = json.loads(
        (cfg.datadir / "weight.json").read_text(encoding="UTF-8")
    )
    assert data["format"]["version"]["major"] == 1

    return [
        TestData(
            weight=item["weight"],
            naive=item["naive"],
            recursive=item["recursive"],
        )
        for item in data["modules"]
    ]


class TestHandler(NamedTuple):
    """ Definition for a single weight mode. """

    name: str
    param: str
    selector: Callable[[TestData], int]


HANDLERS = {
    WeightMode.NAIVE: TestHandler(
        name="naive", param="01-1", selector=lambda item: item.naive
    ),
    WeightMode.RECURSIVE: TestHandler(
        name="recursive", param="01-2", selector=lambda item: item.recursive
    ),
}


def parse_args() -> Config:
    """ Parse the command-line arguments. """
    parser = argparse.ArgumentParser(prog="test_weight")
    parser.add_argument(
        "--playground",
        "-d",
        required=True,
        type=str,
        help="the path to the test playground",
    )
    parser.add_argument(
        "program",
        type=str,
        nargs="+",
        help="the program (and arguments) to test",
    )
    args = parser.parse_args()
    return Config(
        datadir=pathlib.Path("..") / "test-data",
        play=pathlib.Path(args.playground),
        program=list(args.program),
    )


def run(mode: WeightMode) -> None:
    """ Create the data files for all the tests. """
    handler = HANDLERS[mode]
    cfg = parse_args()
    test_data = read_test_data(cfg)
    output = subprocess.check_output(
        cfg.program + [handler.param, str(cfg.play / "test-weight.txt")],
        shell=False,
    ).decode("UTF-8")
    lines = [line.rstrip("\r") for line in output.rstrip("\r\n").split("\n")]
    last_line = lines.pop()

    assert len(lines) == len(
        test_data
    ), f"expected {len(test_data)} intermediate lines, got {len(lines)} ones"
    subtotal = 0
    for item, line in zip(test_data, lines):
        fields = line.split()
        assert len(fields) == 3, f"not three fields in {line}"
        try:
            value, calc, total = [int(data) for data in fields]
        except ValueError:
            assert False, f"not all integer fields in {line}"
        assert value == item.weight, f"unexpected weight in {line}"
        assert calc == handler.selector(
            item
        ), f"unexpected {handler.name} amount in {line}"
        assert total == subtotal + handler.selector(
            item
        ), f"unexpected subtotal in {line}"
        subtotal = total

    assert subtotal == int(last_line), f"unexpected total in {last_line}"
    expected = sum(handler.selector(item) for item in test_data)
    assert subtotal == expected, f"unexpected total: {subtotal} != {expected}"
