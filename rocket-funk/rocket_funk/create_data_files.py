#!/usr/bin/python3
#
# Copyright (c) 2019  Peter Pentchev <roam@ringlet.net>
# All rights reserved.
#
# Redistribution and use in source and binary forms, with or without
# modification, are permitted provided that the following conditions
# are met:
# 1. Redistributions of source code must retain the above copyright
#    notice, this list of conditions and the following disclaimer.
# 2. Redistributions in binary form must reproduce the above copyright
#    notice, this list of conditions and the following disclaimer in the
#    documentation and/or other materials provided with the distribution.
#
# THIS SOFTWARE IS PROVIDED BY THE AUTHOR AND CONTRIBUTORS ``AS IS'' AND
# ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
# IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
# ARE DISCLAIMED.  IN NO EVENT SHALL THE AUTHOR OR CONTRIBUTORS BE LIABLE
# FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
# DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS
# OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
# HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
# LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY
# OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF
# SUCH DAMAGE.
""" Create the data files for all the tests. """

import argparse
import json
import pathlib
import sys

from typing import NamedTuple


class Config(NamedTuple):
    """ Configuration information for the creation of files. """

    datadir: pathlib.Path
    output: pathlib.Path


def create_output_dir(cfg: Config) -> None:
    """ Make sure the output directory exists and is, well, a directory. """
    if cfg.output.is_dir():
        return
    if cfg.output.exists():
        sys.exit(f"Not a directory: {cfg.output}")
    cfg.output.mkdir(parents=True)


def create_m01_weight(cfg: Config) -> None:
    """ Create the test file for the weights calculations. """
    data = json.loads(
        (cfg.datadir / "weight.json").read_text(encoding="UTF-8")
    )
    assert data["format"]["version"]["major"] == 1

    outfile = cfg.output / "test-weight.txt"
    outfile.write_text(
        "".join(f"{item['weight']}\n" for item in data["modules"]),
        encoding="UTF-8",
    )


def create_m02_intcode(cfg: Config) -> None:
    """ Create the test files for the IntCode interpreter. """
    data = json.loads(
        (cfg.datadir / "intcode.json").read_text(encoding="UTF-8")
    )
    assert data["format"]["version"]["major"] == 1

    for index, prog in enumerate(data["intcode"]):
        outfile = cfg.output / f"test-intcode-{index + 1}.txt"
        with outfile.open(mode="wt", encoding="UTF-8") as outf:
            if prog["replace"] is not None:
                print(f"replace={prog['replace']}", file=outf)

            if prog["peek"] != 0:
                print(f"peek={prog['peek']}", file=outf)

            if prog.get("input", []):
                print(
                    "input=" + ",".join(str(value) for value in prog["input"]),
                    file=outf,
                )

            if "seek" in prog:
                print(f"seek={prog['seek']}", file=outf)

            print(",".join(str(value) for value in prog["code"]), file=outf)


def create_m03_wiring(cfg: Config) -> None:
    """ Create the test files for the wire crossings. """
    data = json.loads(
        (cfg.datadir / "wiring.json").read_text(encoding="UTF-8")
    )
    assert data["format"]["version"]["major"] == 1

    for index, case in enumerate(data["wiring"]):
        outfile = cfg.output / f"test-wiring-{index + 1}.txt"
        with outfile.open(mode="wt", encoding="UTF-8") as outf:
            for wire in case["wires"]:
                print(
                    ",".join(
                        item["direction"][0].upper() + str(item["distance"])
                        for item in wire
                    ),
                    file=outf,
                )


def create_m04_password(cfg: Config) -> None:
    """ Create the test files for the wire crossings. """
    data = json.loads(
        (cfg.datadir / "password.json").read_text(encoding="UTF-8")
    )
    assert data["format"]["version"]["major"] == 1

    test_cases = [
        f"{item['first']}-{item['last']}" for item in data["password"]["count"]
    ]
    for index, case in enumerate(test_cases):
        outfile = cfg.output / f"test-password-{index + 1}-lax.txt"
        outfile.write_text(case, encoding="UTF-8")
        outfile = cfg.output / f"test-password-{index + 1}-strict.txt"
        outfile.write_text(case, encoding="UTF-8")


def main() -> None:
    """ Create the data files for all the tests. """
    parser = argparse.ArgumentParser(prog="create_data_files")
    parser.add_argument(
        "--output",
        "-o",
        required=True,
        type=str,
        help="the path to the directory to create the data files in",
    )
    args = parser.parse_args()
    cfg = Config(
        datadir=(pathlib.Path("..") / "test-data"),
        output=pathlib.Path(args.output),
    )

    create_output_dir(cfg)
    create_m01_weight(cfg)
    create_m02_intcode(cfg)
    create_m03_wiring(cfg)
    create_m04_password(cfg)


if __name__ == "__main__":
    main()
