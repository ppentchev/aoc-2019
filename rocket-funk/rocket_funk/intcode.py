# Copyright (c) 2019  Peter Pentchev <roam@ringlet.net>
# All rights reserved.
#
# Redistribution and use in source and binary forms, with or without
# modification, are permitted provided that the following conditions
# are met:
# 1. Redistributions of source code must retain the above copyright
#    notice, this list of conditions and the following disclaimer.
# 2. Redistributions in binary form must reproduce the above copyright
#    notice, this list of conditions and the following disclaimer in the
#    documentation and/or other materials provided with the distribution.
#
# THIS SOFTWARE IS PROVIDED BY THE AUTHOR AND CONTRIBUTORS ``AS IS'' AND
# ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
# IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
# ARE DISCLAIMED.  IN NO EVENT SHALL THE AUTHOR OR CONTRIBUTORS BE LIABLE
# FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
# DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS
# OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
# HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
# LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY
# OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF
# SUCH DAMAGE.
""" Test the IntCode interpreter. """

import argparse
import enum
import json
import pathlib
import subprocess

from typing import Callable, List, NamedTuple, Optional, Set


FEATURE_MAP = {
    "io": "05-1",
    "mode": "05-1",
    "jump-if": "05-2",
}


class Mode(enum.IntEnum):
    """ The run mode for the functional test. """

    RUN = 0
    SEEK = 1
    DIAG = 2


class Config(NamedTuple):
    """ Configuration information for the creation of files. """

    datadir: pathlib.Path
    play: pathlib.Path
    program: List[str]


class TestData(NamedTuple):
    """ A IntCode program. """

    replace: Optional[int]
    code: List[int]
    peek: int
    value: int
    seek: Optional[int]
    features: List[str]
    output: List[int]
    diag: bool


def read_test_data(cfg: Config) -> List[TestData]:
    """ Create the test file for the weights calculations. """
    data = json.loads(
        (cfg.datadir / "intcode.json").read_text(encoding="UTF-8")
    )
    assert data["format"]["version"]["major"] == 1

    return [
        TestData(
            replace=item["replace"],
            code=item["code"],
            peek=item["peek"],
            value=item.get("value"),
            seek=item.get("seek"),
            features=item.get("features", []),
            output=item.get("output", []),
            diag=item.get("diag", False),
        )
        for item in data["intcode"]
    ]


def parse_args() -> Config:
    """ Parse the command-line arguments. """
    parser = argparse.ArgumentParser(prog="test_intcode")
    parser.add_argument(
        "--playground",
        "-d",
        required=True,
        type=str,
        help="the path to the test playground",
    )
    parser.add_argument(
        "program",
        type=str,
        nargs="+",
        help="the program (and arguments) to test",
    )
    args = parser.parse_args()
    return Config(
        datadir=pathlib.Path("..") / "test-data",
        play=pathlib.Path(args.playground),
        program=list(args.program),
    )


class TestHandler(NamedTuple):
    """ A handler for a functional test mode. """

    param: str
    result: Callable[[TestData], Optional[str]]


HANDLERS = {
    Mode.RUN: TestHandler(
        param="02-1", result=lambda data: f"{data.peek} {data.value}"
    ),
    Mode.SEEK: TestHandler(
        param="02-2", result=lambda data: f"{data.replace}"
    ),
    Mode.DIAG: TestHandler(param="05-1", result=lambda data: None),
}


def get_implemented(cfg: Config) -> Set[str]:
    """ Query the program for the tasks it implements. """
    output = subprocess.check_output(
        cfg.program + ["implemented"], shell=False
    ).decode("UTF-8")
    return set(line.rstrip("\r") for line in output.rstrip("\r\n").split("\n"))


def check_output(
    index: int, data: TestData, mode: Mode, lines: List[str]
) -> None:
    """ Validate the program's output instructions. """
    collected_output = []
    for line in lines:
        fields = line.split(" ", 1)
        if len(fields) != 2 or fields[0] != "OUTPUT":
            continue
        try:
            collected_output.append(int(fields[1]))
        except ValueError:
            assert False, (
                f"test {index + 1}: "
                f"not an integer value output: {fields[1]}"
            )
    if mode == mode.DIAG:
        assert collected_output, f"test {index + 1}: no diagnostic output"
        for value in collected_output[:-1]:
            assert value == 0, f"test {index + 1}: non-zero diagnostic {value}"
        assert (
            collected_output[-1] != 0
        ), f"test {index + 1}: zero final diagnostic"
    else:
        assert data.output == collected_output, (
            f"test {index + 1}: "
            f"expected output {data.output}, "
            f"got {collected_output}"
        )


def run(mode: Mode) -> None:
    """ Create the data files for all the tests. """
    handler = HANDLERS[mode]
    cfg = parse_args()
    test_data = read_test_data(cfg)

    implemented: Optional[Set[str]] = None

    for index, data in enumerate(test_data):
        if mode == Mode.RUN and data.value is None:
            continue
        if mode == Mode.SEEK and data.seek is None:
            continue
        if mode == Mode.DIAG and not data.diag:
            continue

        if data.features:
            if implemented is None:
                implemented = get_implemented(cfg)
            missing = [
                feature
                for feature in data.features
                if FEATURE_MAP[feature] not in implemented
            ]
            if missing:
                print(f"  - skipping test {index + 1}: {missing}")
                continue

        output = subprocess.check_output(
            cfg.program
            + [
                handler.param,
                str(cfg.play / f"test-intcode-{index + 1}.txt"),
            ],
            shell=False,
        ).decode("UTF-8")
        lines = [
            line.rstrip("\r") for line in output.rstrip("\r\n").split("\n")
        ]

        check_output(index, data, mode, lines)

        expected_last = handler.result(data)
        if expected_last is not None:
            last_line = lines.pop()
            assert last_line == handler.result(data), (
                f"test {index + 1}: "
                f"expected '{handler.result(data)}', got '{last_line}'"
            )
