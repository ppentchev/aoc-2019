# Copyright (c) 2019  Peter Pentchev <roam@ringlet.net>
# All rights reserved.
#
# Redistribution and use in source and binary forms, with or without
# modification, are permitted provided that the following conditions
# are met:
# 1. Redistributions of source code must retain the above copyright
#    notice, this list of conditions and the following disclaimer.
# 2. Redistributions in binary form must reproduce the above copyright
#    notice, this list of conditions and the following disclaimer in the
#    documentation and/or other materials provided with the distribution.
#
# THIS SOFTWARE IS PROVIDED BY THE AUTHOR AND CONTRIBUTORS ``AS IS'' AND
# ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
# IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
# ARE DISCLAIMED.  IN NO EVENT SHALL THE AUTHOR OR CONTRIBUTORS BE LIABLE
# FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
# DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS
# OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
# HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
# LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY
# OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF
# SUCH DAMAGE.
""" Test the wiring crossings finder. """

import argparse
import enum
import json
import pathlib
import subprocess

from typing import Callable, List, NamedTuple


class Mode(enum.IntEnum):
    """ The run mode for the functional test. """

    CLOSEST = 0
    SHORTEST = 0


class Config(NamedTuple):
    """ Configuration information for the creation of files. """

    datadir: pathlib.Path
    play: pathlib.Path
    program: List[str]


class TestData(NamedTuple):
    """ A wire crossings thing. """

    closest: int
    shortest: int


def read_test_data(cfg: Config) -> List[TestData]:
    """ Create the test file for the weights calculations. """
    data = json.loads(
        (cfg.datadir / "wiring.json").read_text(encoding="UTF-8")
    )
    assert data["format"]["version"]["major"] == 1

    return [
        TestData(closest=item["closest"], shortest=item["shortest"])
        for item in data["wiring"]
    ]


def parse_args() -> Config:
    """ Parse the command-line arguments. """
    parser = argparse.ArgumentParser(prog="test_intcode")
    parser.add_argument(
        "--playground",
        "-d",
        required=True,
        type=str,
        help="the path to the test playground",
    )
    parser.add_argument(
        "program",
        type=str,
        nargs="+",
        help="the program (and arguments) to test",
    )
    args = parser.parse_args()
    return Config(
        datadir=pathlib.Path("..") / "test-data",
        play=pathlib.Path(args.playground),
        program=list(args.program),
    )


class TestHandler(NamedTuple):
    """ A handler for a functional test mode. """

    param: str
    result: Callable[[TestData], str]


HANDLERS = {
    Mode.CLOSEST: TestHandler(
        param="03-1", result=lambda data: f"{data.closest}"
    ),
    Mode.SHORTEST: TestHandler(
        param="03-2", result=lambda data: f"{data.shortest}"
    ),
}


def run(mode: Mode) -> None:
    """ Create the data files for all the tests. """
    handler = HANDLERS[mode]
    cfg = parse_args()
    test_data = read_test_data(cfg)

    for index, data in enumerate(test_data):
        output = subprocess.check_output(
            cfg.program
            + [handler.param, str(cfg.play / f"test-wiring-{index + 1}.txt"),],
            shell=False,
        ).decode("UTF-8")
        lines = [
            line.rstrip("\r") for line in output.rstrip("\r\n").split("\n")
        ]
        last_line = lines.pop()
        assert last_line == handler.result(
            data
        ), f"expected '{handler.result(data)}', got '{last_line}'"
