#!/bin/sh

set -e

funkdir="$(dirname -- "$0")"
cd -- "$funkdir"
python3 -- runfunc.py "$@"
