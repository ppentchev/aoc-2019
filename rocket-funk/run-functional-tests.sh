#!/bin/sh

set -e

tempf="$(mktemp -t run-functional-tests.txt.XXXXXX)"
trap "rm -f -- '$tempf'" EXIT QUIT TERM INT HUP

echo "Checking which tests are implemented by $*"
"$@" implemented > "$tempf"
if [ ! -s "$tempf" ]; then
	echo 'No tasks implemented!' 1>&2
	exit 1
fi
if grep -Eve '^[0-9][0-9]-[0-9]$' -- "$tempf" 1>&2; then
	echo 'Invalid implemented task IDs!' 1>&2
	exit 1
fi
echo "It seems there are $(cat -- "$tempf" | wc -l) implemented tasks"

rm -rf test-playground
python3 -m rocket_funk.create_data_files -o test-playground

while read task_id; do
	test_id="$(printf -- '%s' "$task_id" | tr '-' '_')"
	class_id="rocket_funk.test_$test_id"
	if [ "$task_id" = '02-2' ] && [ "$1" = 'sh' ] && [ -z "$RUN_FULL_SHELL" ]; then
		printf -- '- %s (%s) - SKIPPED (no RUN_FULL_SHELL)\n' "$task_id" "$class_id"
		continue
	fi

	printf -- '- %s (%s)\n' "$task_id" "$class_id"
	python3 -m "$class_id" -d test-playground -- "$@"
done < "$tempf"

echo 'OK!'
