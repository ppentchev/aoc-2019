#!/usr/bin/python3
#
# Copyright (c) 2019  Peter Pentchev <roam@ringlet.net>
# All rights reserved.
#
# Redistribution and use in source and binary forms, with or without
# modification, are permitted provided that the following conditions
# are met:
# 1. Redistributions of source code must retain the above copyright
#    notice, this list of conditions and the following disclaimer.
# 2. Redistributions in binary form must reproduce the above copyright
#    notice, this list of conditions and the following disclaimer in the
#    documentation and/or other materials provided with the distribution.
#
# THIS SOFTWARE IS PROVIDED BY THE AUTHOR AND CONTRIBUTORS ``AS IS'' AND
# ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
# IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
# ARE DISCLAIMED.  IN NO EVENT SHALL THE AUTHOR OR CONTRIBUTORS BE LIABLE
# FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
# DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS
# OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
# HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
# LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY
# OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF
# SUCH DAMAGE.
""" Run all the functional tests. """

import argparse
import os
import pathlib
import subprocess
import sys

from typing import Dict, List, NamedTuple, Set


class ProgramDef(NamedTuple):
    """ A single program's definition. """

    lang: str
    command: List[str]


PROGRAMS = [
    ProgramDef(lang="C", command=["../c/rocket"]),
    ProgramDef(
        lang="Perl", command=["perl", "-I../perl5/lib", "../perl5/rocket.pl"]
    ),
    ProgramDef(
        lang="Python",
        command=["env", "PYTHONPATH=../python", "python3", "-m", "rocket"],
    ),
    ProgramDef(lang="Raku", command=["perl6", "../raku/rocket.p6"]),
    ProgramDef(lang="sh", command=["sh", "../sh/rocket.sh"]),
]


def get_programs() -> List[ProgramDef]:
    """ Get the currently available programs to run. """

    def check_single(path: pathlib.Path) -> bool:
        """ Check a single path for an-executable-file-ness. """
        return (
            path.exists() and path.is_file() and os.access(path, mode=os.X_OK)
        )

    # OK, so this is a bit UNIX/Linux-specific...
    if "PATH" in os.environ:
        search = [
            pathlib.Path(dirname)
            for dirname in os.environ.get("PATH", "").split(":")
        ]
    else:
        search = []

    res = []
    for prog in PROGRAMS:
        # Ooookay, so this is a gross hack
        if prog.command[0] == "env" and "python3" in prog.command:
            cmd = "python3"
        else:
            cmd = prog.command[0]

        if "/" in cmd:
            if check_single(pathlib.Path(cmd)):
                res.append(prog)
            continue

        if any(check_single(path / cmd) for path in search):
            res.append(prog)

    return res


def list_tests() -> None:
    """ List the available implementations. """
    print(
        "\n".join(
            f"{prog.lang}\t{' '.join(prog.command)}" for prog in get_programs()
        )
    )


def run_tests() -> None:
    """ Run the functional tests for all the implementations. """
    for prog in get_programs():
        subprocess.check_call(
            ["./run-functional-tests.sh"] + prog.command, shell=False
        )


def show_matrix() -> None:
    """ Build the matrix of tests vs implementations. """
    all_tasks: Set[str] = set()
    tasks: Dict[str, Set[str]] = {}
    for prog in get_programs():
        output = subprocess.check_output(
            prog.command + ["implemented"], shell=False
        ).decode("UTF-8")
        lines = [
            line.rstrip("\r") for line in output.rstrip("\r\n").split("\n")
        ]
        all_tasks.update(lines)
        tasks[prog.lang] = set(lines)

    all_langs = sorted(tasks.keys())
    print("\t".join(["Task"] + all_langs))
    for task_id in sorted(all_tasks):
        supported = [
            "yes" if task_id in tasks[lang] else "no" for lang in all_langs
        ]
        print("\t".join([task_id] + supported))


def main() -> None:
    """ Run all the functional tests. """
    parser = argparse.ArgumentParser(prog="runfunc")
    subparsers = parser.add_subparsers()

    parser_list = subparsers.add_parser("list")
    parser_list.set_defaults(func=list_tests)

    parser_run = subparsers.add_parser("run")
    parser_run.set_defaults(func=run_tests)

    parser_matrix = subparsers.add_parser("matrix")
    parser_matrix.set_defaults(func=show_matrix)

    args = parser.parse_args()
    if getattr(args, "func", None) is None:
        sys.exit("No action specified: one of 'list', 'matrix', or 'run'")
    args.func()


if __name__ == "__main__":
    main()
