# Copyright (c) 2019  Peter Pentchev <roam@ringlet.net>
# All rights reserved.
#
# Redistribution and use in source and binary forms, with or without
# modification, are permitted provided that the following conditions
# are met:
# 1. Redistributions of source code must retain the above copyright
#    notice, this list of conditions and the following disclaimer.
# 2. Redistributions in binary form must reproduce the above copyright
#    notice, this list of conditions and the following disclaimer in the
#    documentation and/or other materials provided with the distribution.
#
# THIS SOFTWARE IS PROVIDED BY THE AUTHOR AND CONTRIBUTORS ``AS IS'' AND
# ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
# IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
# ARE DISCLAIMED.  IN NO EVENT SHALL THE AUTHOR OR CONTRIBUTORS BE LIABLE
# FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
# DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS
# OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
# HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
# LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY
# OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF
# SUCH DAMAGE.
""" Unit tests for the fuel weight calculation. """

import json
import pathlib
import tempfile
import unittest

from typing import Callable, List, NamedTuple

import ddt  # type: ignore
import mock

from rocket import weight


class TestData(NamedTuple):
    """ A single test data item. """

    weight: int
    naive: int
    recursive: int


_TestData: List[TestData] = []  # pylint: disable=invalid-name


def get_test_data() -> List[TestData]:
    """ Read the test data from a file. """
    global _TestData  # pylint: disable=global-statement,invalid-name
    if _TestData:
        return _TestData

    data = json.loads(
        (pathlib.Path("..") / "test-data" / "weight.json").read_text(
            encoding="UTF-8"
        )
    )
    _TestData = [
        TestData(
            weight=item["weight"],
            naive=item["naive"],
            recursive=item["recursive"],
        )
        for item in data["modules"]
    ]
    return _TestData


@ddt.ddt
class TestWeight(unittest.TestCase):
    """ Test the various weight calculation functions. """

    # pylint: disable=no-self-use

    @ddt.data(*get_test_data())
    def test_naive(self, data: TestData) -> None:
        """ Test the naive calculation. """
        assert weight.calc_fuel(data.weight) == data.naive

    @ddt.data(*get_test_data())
    def test_recursive(self, data: TestData) -> None:
        """ Test the recursive calculation. """
        assert weight.calc_recursive(data.weight) == data.recursive

    def do_test_handle(
        self,
        mode: weight.WeightMode,
        selector: Callable[[TestData], int],
        mock_report_intermediate: mock.MagicMock,
        mock_report_total: mock.MagicMock,
    ) -> None:
        """ Test the calculation of the sum of weights read from a file. """
        test_data = get_test_data()

        with tempfile.NamedTemporaryFile(mode="wt", encoding="UTF-8") as tempf:
            for item in test_data:
                print(item.weight, file=tempf)
            tempf.flush()
            weight.handle(mode=mode, filename=tempf.name)

        assert mock_report_intermediate.call_count == len(test_data)
        for index, (item, called) in enumerate(
            zip(test_data, mock_report_intermediate.call_args_list)
        ):
            assert len(called.args) == 3
            value, needed, total = called.args
            assert value == item.weight
            assert needed == selector(item)
            if index == 0:
                assert total == needed
            else:
                assert total > needed

        mock_report_total.assert_called_once_with(
            sum(selector(item) for item in test_data)
        )

    @mock.patch("rocket.weight.report_total")
    @mock.patch("rocket.weight.report_intermediate")
    def test_handle_naive(
        self,
        mock_report_intermediate: mock.MagicMock,
        mock_report_total: mock.MagicMock,
    ) -> None:
        """ Test handling the naive weight calculation. """
        self.do_test_handle(
            mode=weight.WeightMode.NAIVE,
            selector=lambda data: data.naive,
            mock_report_intermediate=mock_report_intermediate,
            mock_report_total=mock_report_total,
        )

    @mock.patch("rocket.weight.report_total")
    @mock.patch("rocket.weight.report_intermediate")
    def test_handle_recursive(
        self,
        mock_report_intermediate: mock.MagicMock,
        mock_report_total: mock.MagicMock,
    ) -> None:
        """ Test handling the recursive weight calculation. """
        self.do_test_handle(
            mode=weight.WeightMode.RECURSIVE,
            selector=lambda data: data.recursive,
            mock_report_intermediate=mock_report_intermediate,
            mock_report_total=mock_report_total,
        )
