# Copyright (c) 2019  Peter Pentchev <roam@ringlet.net>
# All rights reserved.
#
# Redistribution and use in source and binary forms, with or without
# modification, are permitted provided that the following conditions
# are met:
# 1. Redistributions of source code must retain the above copyright
#    notice, this list of conditions and the following disclaimer.
# 2. Redistributions in binary form must reproduce the above copyright
#    notice, this list of conditions and the following disclaimer in the
#    documentation and/or other materials provided with the distribution.
#
# THIS SOFTWARE IS PROVIDED BY THE AUTHOR AND CONTRIBUTORS ``AS IS'' AND
# ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
# IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
# ARE DISCLAIMED.  IN NO EVENT SHALL THE AUTHOR OR CONTRIBUTORS BE LIABLE
# FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
# DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS
# OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
# HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
# LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY
# OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF
# SUCH DAMAGE.
""" Test the password validity checker. """

import itertools
import json
import pathlib

import unittest

from typing import Dict, List, NamedTuple

import ddt  # type: ignore
import pytest  # type: ignore

from rocket import password


class TestDataMode(NamedTuple):
    """ The test data for a single item for a single mode. """

    valid_count: int
    doubled: bool


class TestData(NamedTuple):
    """ A single test data item. """

    first: str
    last: str
    data: Dict[bool, TestDataMode]


_TestData: List[TestData] = []  # pylint: disable=invalid-name


def get_test_data() -> List[TestData]:
    """ Read the test data from a file. """
    global _TestData  # pylint: disable=global-statement,invalid-name
    if _TestData:
        return _TestData

    data = json.loads(
        (pathlib.Path("..") / "test-data" / "password.json").read_text(
            encoding="UTF-8"
        )
    )
    assert data["format"]["version"]["major"] == 1
    _TestData = [
        TestData(
            first=item["first"],
            last=item["last"],
            data={
                True: TestDataMode(
                    valid_count=item["lax"]["count"],
                    doubled=item["lax"]["doubled"],
                ),
                False: TestDataMode(
                    valid_count=item["strict"]["count"],
                    doubled=item["strict"]["doubled"],
                ),
            },
        )
        for item in data["password"]["count"]
    ]
    return _TestData


def test_seq() -> None:
    """ Test the sequential generator. """
    gen = password.PWGen(first="145784", last="145789")
    value = next(gen)
    assert value == "145788"
    with pytest.raises(StopIteration):
        print(f"\n\nUnexpected: {next(gen)})\n")

    gen = password.PWGen(first="123452", last="123479")
    value = next(gen)
    assert value == "123455"
    value = next(gen)
    assert value == "123466"
    value = next(gen)
    assert value == "123477"
    with pytest.raises(StopIteration):
        print(f"\n\nUnexpected: {next(gen)})\n")


@ddt.ddt
class TestPassword(unittest.TestCase):
    """ Test the password generator. """

    # pylint: disable=no-self-use

    @ddt.data(*itertools.product(get_test_data(), [False, True]))
    @ddt.unpack
    def test_count(self, case: TestData, lax: bool) -> None:
        """ Test the generator for the correct number of passwords. """
        count = password.count_valid(first=case.first, last=case.last, lax=lax)
        assert count == case.data[lax].valid_count

    @ddt.data(*itertools.product(get_test_data(), [False, True]))
    @ddt.unpack
    def test_doubled(self, case: TestData, lax: bool) -> None:
        """ Test whether our check for "properly doubled" works. """
        assert (
            password.check_properly_doubled(
                [int(char) for char in case.first], lax
            )
            == case.data[lax].doubled
        )
