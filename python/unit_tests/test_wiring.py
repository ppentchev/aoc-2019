# Copyright (c) 2019  Peter Pentchev <roam@ringlet.net>
# All rights reserved.
#
# Redistribution and use in source and binary forms, with or without
# modification, are permitted provided that the following conditions
# are met:
# 1. Redistributions of source code must retain the above copyright
#    notice, this list of conditions and the following disclaimer.
# 2. Redistributions in binary form must reproduce the above copyright
#    notice, this list of conditions and the following disclaimer in the
#    documentation and/or other materials provided with the distribution.
#
# THIS SOFTWARE IS PROVIDED BY THE AUTHOR AND CONTRIBUTORS ``AS IS'' AND
# ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
# IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
# ARE DISCLAIMED.  IN NO EVENT SHALL THE AUTHOR OR CONTRIBUTORS BE LIABLE
# FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
# DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS
# OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
# HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
# LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY
# OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF
# SUCH DAMAGE.
""" Test the wiring diagram functionality. """

import json
import pathlib
import unittest

from typing import List, NamedTuple

import ddt  # type: ignore

from rocket import wiring


class WireStep(NamedTuple):
    """ A single step of the wire. """

    direction: str
    distance: int


class TestData(NamedTuple):
    """ A single test data item. """

    wires: List[List[WireStep]]
    closest: int
    shortest: int


_TestData: List[TestData] = []  # pylint: disable=invalid-name


def get_test_data() -> List[TestData]:
    """ Read the test data from a file. """
    global _TestData  # pylint: disable=global-statement,invalid-name
    if _TestData:
        return _TestData

    data = json.loads(
        (pathlib.Path("..") / "test-data" / "wiring.json").read_text(
            encoding="UTF-8"
        )
    )
    assert data["format"]["version"]["major"] == 1
    _TestData = [
        TestData(
            wires=[
                [
                    WireStep(
                        direction=step["direction"], distance=step["distance"]
                    )
                    for step in wire
                ]
                for wire in item["wires"]
            ],
            closest=item["closest"],
            shortest=item["shortest"],
        )
        for item in data["wiring"]
    ]
    return _TestData


def test_make_step() -> None:
    """ Test applying single steps. """
    wire = wiring.Wire(steps=[])
    steps = [
        (wiring.Direction.UP, 5, 0, 5, False, 0, 0, 5),
        (wiring.Direction.RIGHT, 2, 2, 5, True, 5, 0, 2),
        (wiring.Direction.LEFT, 3, -1, 5, True, 5, -1, 2),
        (wiring.Direction.DOWN, 1, -1, 4, False, -1, 4, 5),
        (wiring.Direction.RIGHT, 15, 14, 4, True, 4, -1, 14),
    ]
    pos = wiring.Coords(0, 0)
    for step_data in steps:
        step = wiring.Step(step_data[0], step_data[1])
        npos, nstate = wiring.make_step(pos, wire, step)
        assert (npos, nstate) == (
            wiring.Coords(step_data[2], step_data[3]),
            wiring.StepState(
                wire=wire,
                step=step,
                horiz=step_data[4],
                level=step_data[5],
                lower=step_data[6],
                higher=step_data[7],
            ),
        )
        pos = npos


@ddt.ddt
class TestWiring(unittest.TestCase):
    """ Test the wiring diagram functionality. """

    # pylint: disable=no-self-use

    @ddt.data(*get_test_data())
    def test_closest(self, data: TestData) -> None:
        """ Find the crossing point closest to the origin. """
        wires = [
            wiring.Wire(
                steps=[
                    wiring.Step(
                        direction=wiring.DIRECTION[step.direction],
                        distance=step.distance,
                    )
                    for step in wire
                ]
            )
            for wire in data.wires
        ]
        assert wiring.find_closest(wires, False) == data.closest

    @ddt.data(*get_test_data())
    def test_shortest(self, data: TestData) -> None:
        """ Find the crossing point with the shortest path. """
        wires = [
            wiring.Wire(
                steps=[
                    wiring.Step(
                        direction=wiring.DIRECTION[step.direction],
                        distance=step.distance,
                    )
                    for step in wire
                ]
            )
            for wire in data.wires
        ]
        assert wiring.find_closest(wires, True) == data.shortest
