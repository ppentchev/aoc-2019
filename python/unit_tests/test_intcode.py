# Copyright (c) 2019  Peter Pentchev <roam@ringlet.net>
# All rights reserved.
#
# Redistribution and use in source and binary forms, with or without
# modification, are permitted provided that the following conditions
# are met:
# 1. Redistributions of source code must retain the above copyright
#    notice, this list of conditions and the following disclaimer.
# 2. Redistributions in binary form must reproduce the above copyright
#    notice, this list of conditions and the following disclaimer in the
#    documentation and/or other materials provided with the distribution.
#
# THIS SOFTWARE IS PROVIDED BY THE AUTHOR AND CONTRIBUTORS ``AS IS'' AND
# ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
# IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
# ARE DISCLAIMED.  IN NO EVENT SHALL THE AUTHOR OR CONTRIBUTORS BE LIABLE
# FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
# DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS
# OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
# HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
# LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY
# OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF
# SUCH DAMAGE.
""" Unit tests for the IntCode interpreter. """

import json
import pathlib

import tempfile
import unittest

from typing import List, NamedTuple, Optional

import ddt  # type: ignore
import mock

from rocket import intcode


class TestData(NamedTuple):
    """ A single test data item. """

    replace: Optional[int]
    code: List[int]
    peek: int
    value: Optional[int]
    seek: Optional[int]


_TestData: List[TestData] = []  # pylint: disable=invalid-name


def get_test_data() -> List[TestData]:
    """ Read the test data from a file. """
    global _TestData  # pylint: disable=global-statement,invalid-name
    if _TestData:
        return _TestData

    data = json.loads(
        (pathlib.Path("..") / "test-data" / "intcode.json").read_text(
            encoding="UTF-8"
        )
    )
    assert data["format"]["version"]["major"] == 1
    _TestData = [
        TestData(
            replace=item["replace"],
            code=item["code"],
            peek=item["peek"],
            value=item.get("value"),
            seek=item.get("seek"),
        )
        for item in data["intcode"]
        if not item.get("features")
    ]
    return _TestData


@ddt.ddt
class TestIntCode(unittest.TestCase):
    """ Test the various weight calculation functions. """

    # pylint: disable=no-self-use

    @ddt.data(*[item for item in get_test_data() if item.value is not None])
    def test_run(self, data: TestData) -> None:
        """ Test the naive calculation. """
        print("")  # the Interpreter code will output diagnostic info

        interp = intcode.Interpreter(replace=data.replace, code=data.code)
        assert interp.replace == data.replace
        assert interp.code == data.code
        assert interp.memory == data.code
        assert interp.current == 0

        interp.perform_replacement()
        assert interp.replace == data.replace
        assert interp.code == data.code
        assert len(interp.memory) >= len(data.code)
        assert interp.current == 0

        with mock.patch("rocket.intcode.report_intermediate"):
            interp.run()
        assert interp.replace == data.replace
        assert interp.code == data.code
        assert len(interp.memory) >= len(data.code)
        assert interp.current >= 0

        assert interp.get_value(data.peek) == data.value

    @ddt.data(*[item for item in get_test_data() if item.seek is not None])
    def test_seek(self, data: TestData) -> None:
        """ Test the naive calculation. """
        print("")  # the Interpreter code will output diagnostic info

        assert data.seek is not None
        seeker = intcode.Seeker(seek=data.seek, peek=data.peek, code=data.code)
        with mock.patch(
            "rocket.intcode.report_seeker_intermediate"
        ), mock.patch("rocket.intcode.report_intermediate"):
            seeker.search()
        assert seeker.get_replacement() == data.replace

    @ddt.data(*[item for item in get_test_data() if item.value is not None])
    def test_handle_run(self, prog: TestData) -> None:
        """ Test interpreting some IntCode. """
        with tempfile.NamedTemporaryFile(mode="wt", encoding="UTF-8") as tempf:
            if prog.replace is not None:
                print(f"replace={prog.replace}", file=tempf)

            if prog.peek != 0:
                print(f"peek={prog.peek}", file=tempf)

            print(",".join(str(value) for value in prog.code), file=tempf)
            tempf.flush()

            with mock.patch(
                "rocket.intcode.report_total"
            ) as mock_report_total:
                with mock.patch(
                    "rocket.intcode.report_intermediate"
                ) as mock_report_intermediate:
                    intcode.handle(mode="run", filename=tempf.name)

                    assert mock_report_intermediate.call_count > 0
                    mock_report_total.assert_called_once_with(
                        prog.peek, prog.value
                    )

    @ddt.data(*[item for item in get_test_data() if item.seek is not None])
    def test_handle_seek(self, prog: TestData) -> None:
        """ Test seeking for nouns and verbs. """
        with tempfile.NamedTemporaryFile(mode="wt", encoding="UTF-8") as tempf:
            print(f"seek={prog.seek}", file=tempf)

            if prog.peek != 0:
                print(f"peek={prog.peek}", file=tempf)

            print(",".join(str(value) for value in prog.code), file=tempf)
            tempf.flush()

            with mock.patch(
                "rocket.intcode.report_seeker_total"
            ) as mock_report_total:
                with mock.patch(
                    "rocket.intcode.report_seeker_intermediate"
                ) as mock_report_intermediate, mock.patch(
                    "rocket.intcode.report_intermediate"
                ):
                    intcode.handle(mode="seek", filename=tempf.name)

                    assert mock_report_intermediate.call_count >= 0
                    mock_report_total.assert_called_once_with(prog.replace)
