# Copyright (c) 2019  Peter Pentchev <roam@ringlet.net>
# All rights reserved.
#
# Redistribution and use in source and binary forms, with or without
# modification, are permitted provided that the following conditions
# are met:
# 1. Redistributions of source code must retain the above copyright
#    notice, this list of conditions and the following disclaimer.
# 2. Redistributions in binary form must reproduce the above copyright
#    notice, this list of conditions and the following disclaimer in the
#    documentation and/or other materials provided with the distribution.
#
# THIS SOFTWARE IS PROVIDED BY THE AUTHOR AND CONTRIBUTORS ``AS IS'' AND
# ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
# IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
# ARE DISCLAIMED.  IN NO EVENT SHALL THE AUTHOR OR CONTRIBUTORS BE LIABLE
# FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
# DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS
# OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
# HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
# LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY
# OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF
# SUCH DAMAGE.
""" Figure out the total amount of fuel needed for the launch. """

import argparse
import re

from typing import Callable, Dict, Iterable

from rocket import weight
from rocket import intcode
from rocket import wiring
from rocket import password


RE_TEST_ID = re.compile(
    r"""
    ^
    [0-9][0-9] - [0-9]
    $""",
    re.X,
)


def list_implemented(skeys: Iterable[str]) -> None:
    """ List the implemented tasks. """
    tests = sorted(item for item in skeys if RE_TEST_ID.match(item))
    print("\n".join(tests))


def main() -> None:
    """ Get the modules' weight, calculate the amount of fuel needed. """
    parser = argparse.ArgumentParser(prog="rocket")
    subparsers = parser.add_subparsers(help="daily challenge commands")

    parser_implemented = subparsers.add_parser(
        "implemented", help="list the implemented tests"
    )
    parser_implemented.set_defaults(mode="implemented")

    parser_01_1 = subparsers.add_parser("01-1", help="naive fuel weight")
    parser_01_1.add_argument(
        "weightfile",
        type=str,
        help="the name of the file containing the weights",
    )
    parser_01_1.set_defaults(
        mode="weight", weight_mode=weight.WeightMode.NAIVE
    )

    parser_01_2 = subparsers.add_parser("01-2", help="recursive fuel weight")
    parser_01_2.add_argument(
        "weightfile",
        type=str,
        help="the name of the file containing the weights",
    )
    parser_01_2.set_defaults(
        mode="weight", weight_mode=weight.WeightMode.RECURSIVE
    )

    parser_02_1 = subparsers.add_parser("02-1", help="simple IntCode")
    parser_02_1.add_argument(
        "programfile",
        type=str,
        help="the name of the file containing the IntCode program",
    )
    parser_02_1.set_defaults(mode="intcode", intcode_mode="run")

    parser_02_2 = subparsers.add_parser("02-2", help="simple IntCode")
    parser_02_2.add_argument(
        "programfile",
        type=str,
        help="the name of the file containing the IntCode program",
    )
    parser_02_2.set_defaults(mode="intcode", intcode_mode="seek")

    parser_03_1 = subparsers.add_parser("03-1", help="find crossed wires")
    parser_03_1.add_argument(
        "wiresfile",
        type=str,
        help="the name of the file containing the wire definitions",
    )
    parser_03_1.set_defaults(mode="wiring", wiring_mode="closest")

    parser_03_2 = subparsers.add_parser(
        "03-2", help="find shortest signal path"
    )
    parser_03_2.add_argument(
        "wiresfile",
        type=str,
        help="the name of the file containing the wire definitions",
    )
    parser_03_2.set_defaults(mode="wiring", wiring_mode="shortest")

    parser_04_1 = subparsers.add_parser("04-1", help="count passwords")
    parser_04_1.add_argument(
        "rangefile",
        type=str,
        help="the name of the file containing the password range",
    )
    parser_04_1.set_defaults(mode="password", password_mode="lax")

    parser_04_2 = subparsers.add_parser("04-2", help="count passwords strict")
    parser_04_2.add_argument(
        "rangefile",
        type=str,
        help="the name of the file containing the password range",
    )
    parser_04_2.set_defaults(mode="password", password_mode="strict")

    args = parser.parse_args()

    handlers: Dict[str, Callable[[], None]] = {
        "implemented": lambda: list_implemented(subparsers.choices.keys()),
        "intcode": lambda: intcode.handle(
            mode=args.intcode_mode, filename=args.programfile
        ),
        "password": lambda: password.handle(
            mode=args.password_mode, filename=args.rangefile
        ),
        "weight": lambda: weight.handle(
            mode=args.weight_mode, filename=args.weightfile
        ),
        "wiring": lambda: wiring.handle(
            mode=args.wiring_mode, filename=args.wiresfile
        ),
    }
    handlers[args.mode]()


if __name__ == "__main__":
    main()
