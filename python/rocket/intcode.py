# Copyright (c) 2019  Peter Pentchev <roam@ringlet.net>
# All rights reserved.
#
# Redistribution and use in source and binary forms, with or without
# modification, are permitted provided that the following conditions
# are met:
# 1. Redistributions of source code must retain the above copyright
#    notice, this list of conditions and the following disclaimer.
# 2. Redistributions in binary form must reproduce the above copyright
#    notice, this list of conditions and the following disclaimer in the
#    documentation and/or other materials provided with the distribution.
#
# THIS SOFTWARE IS PROVIDED BY THE AUTHOR AND CONTRIBUTORS ``AS IS'' AND
# ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
# IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
# ARE DISCLAIMED.  IN NO EVENT SHALL THE AUTHOR OR CONTRIBUTORS BE LIABLE
# FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
# DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS
# OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
# HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
# LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY
# OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF
# SUCH DAMAGE.
""" An interpreter for IntCode. """

import enum
import pathlib
import sys

from typing import Callable, Dict, List, Optional

from mypy_extensions import TypedDict


class OpCode(enum.IntEnum):
    """ The supported operation codes. """

    ADD = 1
    MULTIPLY = 2

    END = 99


class OpState(TypedDict, total=False):
    """ An attempt to execute an operation. """

    current: int
    opcode: OpCode
    offset_1: int
    offset_2: int
    op1: int
    op2: int
    result: int
    store: int


def report_intermediate(state: OpState) -> None:
    """ Report a step in the program's execution. """
    print(repr(state))


def report_total(offset: int, value: int) -> None:
    """ Report the final result. """
    print(f"{offset} {value}")


class Interpreter:
    """ An interpreter for IntCode. """

    replace: Optional[int]
    code: List[int]
    memory: List[int]
    current: int

    OPERATIONS: Dict[OpCode, Callable[[int, int], int]] = {
        OpCode.ADD: lambda op1, op2: op1 + op2,
        OpCode.MULTIPLY: lambda op1, op2: op1 * op2,
    }

    def __init__(self, replace: Optional[int], code: List[int]) -> None:
        """ Initialize an IntCode interpreter's state. """
        self.replace = replace
        self.code = code
        self.memory = list(code)
        self.current = 0

    def perform_replacement(self) -> None:
        """ Perform the pre-run value replacement. """
        if self.replace is None:
            return

        noun, verb = self.replace // 100, self.replace % 100
        self.set_value(1, noun)
        self.set_value(2, verb)

    def run(self) -> None:
        """ Execute the program. """
        while True:
            opcode = OpCode(self.get_value(self.current))
            if opcode == OpCode.END:
                report_intermediate(
                    OpState(current=self.current, opcode=opcode)
                )
                return

            state = OpState(
                current=self.current,
                opcode=opcode,
                offset_1=self.get_value(self.current + 1),
                offset_2=self.get_value(self.current + 2),
                store=self.get_value(self.current + 3),
            )
            state["op1"] = self.get_value(state["offset_1"])
            state["op2"] = self.get_value(state["offset_2"])
            state["result"] = self.OPERATIONS[state["opcode"]](
                state["op1"], state["op2"]
            )
            report_intermediate(state)
            self.set_value(state["store"], state["result"])
            self.current += 4

    def get_value(self, offset: int) -> int:
        """ Get a value from the interpreter's memory. """
        if offset < 0:
            raise IndexError("The offset must be non-negative")
        if offset >= len(self.memory):
            return 0
        return self.memory[offset]

    def set_value(self, offset: int, value: int) -> None:
        """ Change a value in the interpreter's memory. """
        if offset < 0:
            raise IndexError("The offset must be non-negative")
        if offset >= len(self.memory):
            self.memory.extend([0] * (offset - len(self.memory)))
        self.memory[offset] = value


def report_seeker_intermediate(
    noun: int, verb: int, value: int, seek: int
) -> None:
    """ Report the result of a single seeker test. """
    print(f"noun {noun} verb {verb} value {value} sought {seek}")


def report_seeker_total(replace: int) -> None:
    """ Report the result of a search for a noun and a verb. """
    print(replace)


class Seeker:
    """ Find a noun and verb for the specified result value. """

    code: List[int]
    peek: int
    seek: int
    replacement: Optional[int]

    def __init__(self, seek: int, peek: int, code: List[int]) -> None:
        """ Initialize a seeker. """
        self.seek = seek
        self.peek = peek
        self.code = code
        self.replacement = None

    def search(self) -> None:
        """ Perform the actual search. """
        for noun in range(len(self.code) + 1):
            for verb in range(len(self.code) + 1):
                replace = noun * 100 + verb
                interp = Interpreter(replace=replace, code=self.code)

                interp.perform_replacement()
                interp.run()
                value = interp.get_value(self.peek)
                report_seeker_intermediate(noun, verb, value, self.seek)

                if value == self.seek:
                    self.replacement = replace
                    return

        self.replacement = -1

    def get_replacement(self) -> Optional[int]:
        """ Get the result of the search. """
        return self.replacement


def handle(mode: str, filename: str) -> None:
    """ Read a file, execute the program, output the result. """
    full = pathlib.Path(filename).read_text(encoding="UTF-8")
    lines = [line.strip("\r") for line in full.rstrip("\r\n").split("\n")]
    replace = None
    peek = 0
    seek = 0
    code = []
    for line in lines:
        fields = line.split("=", 1)
        if len(fields) == 2:
            if fields[0] == "replace":
                replace = int(fields[1])
                if replace < -1:
                    sys.exit("The replace mode must be non-negative or -1")
            elif fields[0] == "peek":
                peek = int(fields[1])
                if peek < 0:
                    sys.exit("The peek offset must be non-negative")
            elif fields[0] == "seek":
                seek = int(fields[1])
                if seek < 0:
                    sys.exit("The seek value must be non-negative")
            else:
                sys.exit(f"Unknown variable '{fields[0]}'")
            continue

        values = [int(field) for field in line.split(",")]
        code.extend(values)

    if mode == "run":
        interp = Interpreter(replace=replace, code=code)
        interp.perform_replacement()
        interp.run()
        report_total(peek, interp.get_value(peek))
    else:
        seeker = Seeker(seek=seek, peek=peek, code=code)
        seeker.search()
        replace = seeker.get_replacement()
        assert replace is not None
        report_seeker_total(replace)
