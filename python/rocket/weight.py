# Copyright (c) 2019  Peter Pentchev <roam@ringlet.net>
# All rights reserved.
#
# Redistribution and use in source and binary forms, with or without
# modification, are permitted provided that the following conditions
# are met:
# 1. Redistributions of source code must retain the above copyright
#    notice, this list of conditions and the following disclaimer.
# 2. Redistributions in binary form must reproduce the above copyright
#    notice, this list of conditions and the following disclaimer in the
#    documentation and/or other materials provided with the distribution.
#
# THIS SOFTWARE IS PROVIDED BY THE AUTHOR AND CONTRIBUTORS ``AS IS'' AND
# ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
# IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
# ARE DISCLAIMED.  IN NO EVENT SHALL THE AUTHOR OR CONTRIBUTORS BE LIABLE
# FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
# DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS
# OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
# HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
# LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY
# OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF
# SUCH DAMAGE.
""" Calculate the weight of the fuel needed for the flight. """

import enum
import pathlib


class WeightMode(enum.IntEnum):
    """ The mode in which the fuel needs are calculated. """

    NAIVE = 0
    RECURSIVE = 1


def calc_fuel(value: int) -> int:
    """ Calculate the fuel needed for a single module. """
    return int(value / 3) - 2


def calc_recursive(value: int) -> int:
    """ Recursively calculate the total fuel needed for a single module. """
    total = 0
    while True:
        needed = calc_fuel(value)
        if needed <= 0:
            break
        total += needed
        value = needed

    return total


def report_intermediate(value: int, needed: int, total: int) -> None:
    """ Report the result of a single module's calculation. """
    print(f"{value} {needed} {total}")


def report_total(total: int) -> None:
    """ Report the full calculation result. """
    print(total)


def handle(mode: WeightMode, filename: str) -> None:
    """ Naive calculation of the fuel weight. """
    handlers = {
        WeightMode.NAIVE: calc_fuel,
        WeightMode.RECURSIVE: calc_recursive,
    }

    weightfile = pathlib.Path(filename)
    data = weightfile.read_text(encoding="UTF-8")

    calc = handlers[mode]
    total = 0
    for line in data.rstrip().split("\n"):
        value = int(line.strip())
        needed = calc(value)
        total += needed
        report_intermediate(value, needed, total)

    report_total(total)
