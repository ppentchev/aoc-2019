# Copyright (c) 2019  Peter Pentchev <roam@ringlet.net>
# All rights reserved.
#
# Redistribution and use in source and binary forms, with or without
# modification, are permitted provided that the following conditions
# are met:
# 1. Redistributions of source code must retain the above copyright
#    notice, this list of conditions and the following disclaimer.
# 2. Redistributions in binary form must reproduce the above copyright
#    notice, this list of conditions and the following disclaimer in the
#    documentation and/or other materials provided with the distribution.
#
# THIS SOFTWARE IS PROVIDED BY THE AUTHOR AND CONTRIBUTORS ``AS IS'' AND
# ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
# IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
# ARE DISCLAIMED.  IN NO EVENT SHALL THE AUTHOR OR CONTRIBUTORS BE LIABLE
# FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
# DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS
# OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
# HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
# LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY
# OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF
# SUCH DAMAGE.
""" A password generator of sorts. """

import pathlib
import sys

from typing import Iterator, List, NamedTuple


class CharacterState(NamedTuple):
    """ How to proceed with a single password character. """

    v_min: int
    v_max: int
    at_min: bool
    at_max: bool


def check_properly_doubled(num: List[int], lax: bool) -> bool:
    """ Check whether the double condition is satisfied. """
    seq = 0
    last = None
    for char in num:
        if char == last:
            seq += 1
            if seq == 2 and lax:
                return True
        else:
            if seq == 2:
                return True
            seq = 1

        last = char

    return seq == 2


class PWGen(Iterator[str]):
    """ A password generator or something. """

    # pylint: disable=too-many-instance-attributes

    first: str
    last: str
    i_first: List[int]
    i_last: List[int]
    length: int
    lax: bool
    done: bool

    current: List[int]
    state: List[CharacterState]
    yielded: bool

    def __init__(self, first: str, last: str, lax: bool = True) -> None:
        """ Initialize the generator. """
        if len(first) != len(last):
            raise ValueError(
                "A range must consist of strings of the same length"
            )
        if not first:
            raise ValueError("A range must consist of non-empty strings")
        if first > last:
            self.done = False
            return

        i_first = [int(char) for char in first]
        i_last = [int(char) for char in last]

        self.first = first
        self.last = last
        self.i_first = i_first
        self.i_last = i_last
        self.lax = lax
        self.length = len(i_first)
        self.done = self.first > self.last

        self.current = []
        self.state = []
        self.yielded = False

        if not self.done:
            self.push_first()

    def __iter__(self) -> "PWGen":
        """ Return the iterator object itself. """
        return self

    def push_first(self) -> None:
        """ Start the thing and stuff. """
        assert not self.done
        first_state = CharacterState(
            v_min=self.i_first[0],
            v_max=self.i_last[0],
            at_min=True,
            at_max=True,
        )
        if not self.push(first_state):
            # Not even a single one?
            self.done = True

    def push(self, state: CharacterState) -> bool:
        """ Start processing the next digit. """
        idx = len(self.current)
        assert len(self.state) == idx

        if state.v_min > state.v_max:
            return False

        self.state.append(state)
        return self.try_character(state.v_min)

    def try_character(self, char: int) -> bool:
        """ Try to place the specified character and push on. """
        idx = len(self.current)
        assert len(self.state) == idx + 1
        state = self.state[-1]
        assert state.v_min <= char <= state.v_max

        self.current.append(char)
        if idx == self.length - 1:
            return (
                check_properly_doubled(self.current, self.lax)
                or self.try_next()
            )

        is_first = char == self.i_first[idx]
        is_last = char == self.i_last[idx]
        next_state = CharacterState(
            v_min=max(char, self.i_first[idx + 1])
            if state.at_min and is_first
            else char,
            v_max=self.i_last[idx + 1] if state.at_max and is_last else 9,
            at_min=state.at_min and is_first,
            at_max=state.at_max and is_last,
        )
        return self.push(next_state) or self.try_next()

    def try_next(self) -> bool:
        """ Try to increment the current character, see where it gets us. """
        current_char = self.current.pop()
        idx = len(self.current)
        assert len(self.state) == idx + 1
        state = self.state[-1]

        # No more?
        if current_char == state.v_max:
            self.state.pop()
            return False

        return self.try_character(current_char + 1)

    def get_current(self) -> str:
        """ Get a string representation of the last found valid password. """
        assert len(self.current) == self.length
        return "".join(str(digit) for digit in self.current)

    def __next__(self) -> str:
        """ Return the next valid password. """
        if self.done:
            raise StopIteration()

        if not self.yielded:
            self.yielded = True
            return self.get_current()

        while self.state:
            if self.try_next():
                return self.get_current()

        self.done = True
        raise StopIteration()


def count_valid(first: str, last: str, lax: bool = True) -> int:
    """ Count the valid passwords in the specified range. """
    gen = PWGen(first=first, last=last, lax=lax)
    count = 0
    while True:
        try:
            next(gen)
            count += 1
        except StopIteration:
            return count


def report_count(valid_count: int) -> None:
    """ Output the counted valid passwords. """
    print(valid_count)


def handle(mode: str, filename: str) -> None:
    """ Read a file, execute the program, output the result. """
    full = pathlib.Path(filename).read_text(encoding="UTF-8")
    lines = [line.strip("\r") for line in full.rstrip("\r\n").split("\n")]
    if len(lines) != 1:
        sys.exit("Expected a single first-last line in the range file")
    fields = lines[0].split("-")
    if len(fields) != 2:
        sys.exit("Expected first-last on the single line in the range file")
    if mode in ("lax", "strict"):
        report_count(count_valid(fields[0], fields[1], lax=mode == "lax"))
    else:
        raise NotImplementedError()
