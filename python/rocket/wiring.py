# Copyright (c) 2019  Peter Pentchev <roam@ringlet.net>
# All rights reserved.
#
# Redistribution and use in source and binary forms, with or without
# modification, are permitted provided that the following conditions
# are met:
# 1. Redistributions of source code must retain the above copyright
#    notice, this list of conditions and the following disclaimer.
# 2. Redistributions in binary form must reproduce the above copyright
#    notice, this list of conditions and the following disclaimer in the
#    documentation and/or other materials provided with the distribution.
#
# THIS SOFTWARE IS PROVIDED BY THE AUTHOR AND CONTRIBUTORS ``AS IS'' AND
# ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
# IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
# ARE DISCLAIMED.  IN NO EVENT SHALL THE AUTHOR OR CONTRIBUTORS BE LIABLE
# FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
# DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS
# OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
# HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
# LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY
# OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF
# SUCH DAMAGE.
""" Find the closest crossing point of two wires. """

import enum
import pathlib

from typing import Dict, List, NamedTuple, Optional, Tuple


class Direction(enum.IntEnum):
    """ A single step's direction. """

    UP = 0
    RIGHT = 1
    DOWN = 2
    LEFT = 3

    def is_horizontal(self) -> bool:
        """ Determine whether the direction is horizontal. """
        return self in (self.RIGHT, self.LEFT)


DIRECTION: Dict[str, Direction] = {
    "up": Direction.UP,
    "right": Direction.RIGHT,
    "down": Direction.DOWN,
    "left": Direction.LEFT,
}

DIRECTION.update({name[0].upper(): value for name, value in DIRECTION.items()})


class Coords(NamedTuple):
    """ The coordinates of a single point. """

    x: int
    y: int


COORDS: Dict[Direction, Coords] = {
    Direction.UP: Coords(0, 1),
    Direction.RIGHT: Coords(1, 0),
    Direction.DOWN: Coords(0, -1),
    Direction.LEFT: Coords(-1, 0),
}


class Step(NamedTuple):
    """ A single step in a wire. """

    direction: Direction
    distance: int


class Wire(NamedTuple):
    """ The definition of a single wire. """

    steps: List[Step]


class StepState(NamedTuple):
    """ The processed state of a wire for easy matching. """

    wire: Wire
    step: Step
    horiz: bool
    level: int
    lower: int
    higher: int


def make_step(pos: Coords, wire: Wire, step: Step) -> Tuple[Coords, StepState]:
    """ Calculate the position and state after a step. """
    delta = COORDS[step.direction]
    nextpos = Coords(
        pos.x + delta.x * step.distance, pos.y + delta.y * step.distance,
    )

    if step.direction.is_horizontal():
        assert pos.x != nextpos.x
        assert pos.y == nextpos.y
        current = StepState(
            wire=wire,
            step=step,
            horiz=True,
            level=pos.y,
            lower=min(pos.x, nextpos.x),
            higher=max(pos.x, nextpos.x),
        )
    else:
        assert pos.x == nextpos.x
        assert pos.y != nextpos.y
        current = StepState(
            wire=wire,
            step=step,
            horiz=False,
            level=pos.x,
            lower=min(pos.y, nextpos.y),
            higher=max(pos.y, nextpos.y),
        )

    return nextpos, current


def distance(wire: Wire, point: Coords) -> int:
    """ Find the length of the path from the origin to this point. """

    pos = Coords(0, 0)

    def real_distance_along(
        start: int, lower: int, higher: int, reverse: bool
    ) -> int:
        """ How far along is this point then? """
        if start < lower or start > higher:
            return -1

        value = start - lower
        return (higher - lower) - value if reverse else value

    def distance_along(state: StepState) -> int:
        """ Is "point" on the step from "pos" along "state"? """
        if state.horiz:
            if point.y != state.level:
                return -1
            assert point.y == pos.y
            return real_distance_along(
                point.x,
                state.lower,
                state.higher,
                state.step.direction == Direction.LEFT,
            )

        if point.x != state.level:
            return -1
        assert point.x == pos.x
        return real_distance_along(
            point.y,
            state.lower,
            state.higher,
            state.step.direction == Direction.DOWN,
        )

    res = 0
    for step in wire.steps:
        nextpos, state = make_step(pos, wire, step)
        current = distance_along(state)
        if current >= 0:
            # We reached the point with this step
            return res + current

        res += state.higher - state.lower
        pos = nextpos

    assert False, "Internal error: point not on the wire"


def find_crosspoints(
    seen: Dict[bool, List[StepState]], current: StepState, calc_distance: bool
) -> List[int]:
    """ Find the distances from the crossing points to the origin. """
    res = []
    for state in seen[not current.horiz]:
        if state.wire == current.wire:
            continue
        if current.level < state.lower or current.level > state.higher:
            continue
        if state.level < current.lower or state.level > current.higher:
            continue

        if calc_distance:
            if current.horiz:
                crossing = Coords(state.level, current.level)
            else:
                crossing = Coords(current.level, state.level)
            res.append(
                distance(current.wire, crossing)
                + distance(state.wire, crossing)
            )
        else:
            res.append(abs(current.level) + abs(state.level))

    return res


def find_closest(wires: List[Wire], calc_distance: bool) -> Optional[int]:
    """ Find the minimum distance from a crossing point and the origin. """
    seen: Dict[bool, List[StepState]] = {False: [], True: []}
    crossings = set()
    for wire_index, wire in enumerate(wires):
        pos = Coords(0, 0)
        for step_index, step in enumerate(wire.steps):
            nextpos, current = make_step(pos, wire, step)
            points = find_crosspoints(seen, current, calc_distance)
            if wire_index == 0:
                assert points == [], f"Internal error: first wire: {points}"
            else:
                # Most of the wires cross at the origin, but it does not count
                if step_index == 0 and 0 in points:
                    points.remove(0)

                crossings.update(points)
                # Only leave a single one
                if crossings:
                    crossings = set([min(crossings)])

            seen[current.horiz].append(current)
            pos = nextpos

    if crossings:
        # There can be only one!
        assert len(crossings) == 1, f"Internal error: {repr(crossings)}"
        return min(crossings)
    return None


def report_closest(value: Optional[int]) -> None:
    """ Report the closest crossing point for the wires. """
    print(value if value is not None else -1)


def handle(mode: str, filename: str) -> None:
    """ Read a file, execute the program, output the result. """
    full = pathlib.Path(filename).read_text(encoding="UTF-8")
    lines = [line.strip("\r") for line in full.rstrip("\r\n").split("\n")]
    wires = [
        Wire(
            steps=[
                Step(direction=DIRECTION[item[0]], distance=int(item[1:]))
                for item in line.split(",")
            ]
        )
        for line in lines
    ]

    if mode in ("closest", "shortest"):
        report_closest(find_closest(wires, mode == "shortest"))
    else:
        raise NotImplementedError()
