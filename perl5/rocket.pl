#!/usr/bin/perl
#
# Copyright (c) 2019  Peter Pentchev
# All rights reserved.
#
# Redistribution and use in source and binary forms, with or without
# modification, are permitted provided that the following conditions
# are met:
# 1. Redistributions of source code must retain the above copyright
#    notice, this list of conditions and the following disclaimer.
# 2. Redistributions in binary form must reproduce the above copyright
#    notice, this list of conditions and the following disclaimer in the
#    documentation and/or other materials provided with the distribution.
#
# THIS SOFTWARE IS PROVIDED BY THE AUTHOR AND CONTRIBUTORS ``AS IS'' AND
# ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
# IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
# ARE DISCLAIMED.  IN NO EVENT SHALL THE AUTHOR OR CONTRIBUTORS BE LIABLE
# FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
# DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS
# OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
# HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
# LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY
# OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF
# SUCH DAMAGE.

use v5.10;
use strict;
use warnings;

use Getopt::Std;

use Rocket::Weight;

use Rocket::IntCode;
use Rocket::IntCode::Interpreter;

use constant VERSION_STRING => '0.1.0';

my $debug = 0;

sub usage($)
{
	my ($err) = @_;
	my $s = <<EOUSAGE
Usage:	rocket 01-1 weights-list.txt
	rocket 01-2 weights-list.txt
	rocket 02-1 intcode-program.txt
	rocket implemented
	rocket -V | -h | --version | --help
	rocket --features

	-h	display program usage information and exit
	-V	display program version information and exit
EOUSAGE
	;

	if ($err) {
		die $s;
	} else {
		print "$s";
	}
}

sub version()
{
	say 'rocket '.VERSION_STRING;
}

sub features()
{
	say 'Features: rocket='.VERSION_STRING;
}

sub debug($)
{
	say STDERR "RDBG $_[0]" if $debug;
}

sub help_or_version($)
{
	my ($opts) = @_;
	my $has_dash = defined $opts->{'-'};
	my $dash_help = $has_dash && $opts->{'-'} eq 'help';
	my $dash_version = $has_dash && $opts->{'-'} eq 'version';
	my $dash_features = $has_dash && $opts->{'-'} eq 'features';
	
	if ($has_dash && !$dash_help && !$dash_version && !$dash_features) {
		warn "Invalid long option '".$opts->{'-'}."' specified\n";
		usage 1;
	}
	version if $opts->{V} || $dash_version;
	usage 0 if $opts->{h} || $dash_help;
	features if $dash_features;
	exit 0 if $opts->{V} || $opts->{h} || $has_dash;
}

sub run_weights($ $)
{
	my ($filename, $calc) = @_;

	open(my $f, '<', $filename) or die "Could not open $filename: $!\n";
	my $total = 0;
	while (my $line = <$f>) {
		chomp $line;
		my $value = $calc->($line);
		$total += $value;
		say "$line $value $total";
	}
	say $total;
}

sub run_weights_naive($ @)
{
	my ($cmd, @args) = @_;

	die "Need a single filename\n" unless @args == 1;
	my ($filename) = @args;

	run_weights $filename, \&Rocket::Weight::calc_fuel;
}

sub run_weights_recursive($ @)
{
	my ($cmd, @args) = @_;

	die "Need a single filename\n" unless @args == 1;
	my ($filename) = @args;

	run_weights $filename, \&Rocket::Weight::calc_fuel_recursive;
}

sub run_intcode_run($ @)
{
	my ($cmd, @args) = @_;

	die "Need a single filename\n" unless @args == 1;
	my ($filename) = @args;

	my $data = Rocket::IntCode::load($filename);
	my $interp = Rocket::IntCode::Interpreter->new(
		replace => $data->{replace},
		code => $data->{code},
	);
	$interp->initialize;
	$interp->perform_replacement;
	$interp->run;
	my $peek = $data->{peek} // 0;
	my $value = $interp->get_value($peek);
	say "$peek $value";
}

sub run_intcode_seek($ @)
{
	my ($cmd, @args) = @_;

	die "Need a single filename\n" unless @args == 1;
	my ($filename) = @args;

	my $data = Rocket::IntCode::load($filename);
	my $max = @{$data->{code}};

	for my $noun (0..$max) {
		for my $verb (0..$max) {
			say "trying noun $noun verb $verb";
			my $replace = $noun * 100 + $verb;
			my $interp = Rocket::IntCode::Interpreter->new(
				replace => $replace,
				code => $data->{code},
			);
			$interp->initialize;
			$interp->perform_replacement;
			$interp->run;
			my $peek = $data->{peek} // 0;
			my $value = $interp->get_value($peek);
			say "noun $noun verb $verb value $value/$data->{seek}";
			if ($value == $data->{seek}) {
				say $replace;
				return;
			}
		}
	}
	say -1;
}

sub show_implemented($ @);

my %handlers = (
	'01-1' => \&run_weights_naive,
	'01-2' => \&run_weights_recursive,
	'02-1' => \&run_intcode_run,
	'02-2' => \&run_intcode_seek,

	'implemented' => \&show_implemented,
);

sub show_implemented($ @)
{
	my ($cmd, @args) = @_;

	die "No arguments to 'implemented'\n" if @args;
	say for sort grep /^[0-9][0-9]-[0-9]$/, keys %handlers;
}

MAIN:
{
	my %opts;

	getopts('hV-:', \%opts) or usage 1;
	help_or_version \%opts;
	$debug = $opts{v};

	usage 1 unless @ARGV;
	my ($cmd, @args) = @ARGV;
	$handlers{$cmd}->($cmd, @args);
}
