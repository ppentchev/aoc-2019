#!/usr/bin/perl
#
# Copyright (c) 2019  Peter Pentchev <roam@ringlet.net>
# All rights reserved.
#
# Redistribution and use in source and binary forms, with or without
# modification, are permitted provided that the following conditions
# are met:
# 1. Redistributions of source code must retain the above copyright
#    notice, this list of conditions and the following disclaimer.
# 2. Redistributions in binary form must reproduce the above copyright
#    notice, this list of conditions and the following disclaimer in the
#    documentation and/or other materials provided with the distribution.
#
# THIS SOFTWARE IS PROVIDED BY THE AUTHOR AND CONTRIBUTORS ``AS IS'' AND
# ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
# IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
# ARE DISCLAIMED.  IN NO EVENT SHALL THE AUTHOR OR CONTRIBUTORS BE LIABLE
# FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
# DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS
# OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
# HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
# LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY
# OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF
# SUCH DAMAGE.

use v5.012;
use strict;
use warnings;

use JSON::XS qw(decode_json);
use Path::Tiny;
use Test::More;

use Rocket::IntCode::Interpreter;

my $data = decode_json path('../test-data/intcode.json')->slurp_utf8;
BAIL_OUT 'Unrecognized test data format' unless
    $data->{format}{version}{major} == 1;

plan tests => scalar @{$data->{intcode}};

for my $prog (@{$data->{intcode}}) {
	my $replace = $prog->{replace} // '(none)';
	subtest "program @{$prog->{code}} replace $replace" => sub {
		plan tests => 5;

		SKIP: {
			skip 'Not runnable', 5 unless defined $prog->{value};

			my $interp = Rocket::IntCode::Interpreter->new(
				replace => $prog->{replace},
				code => $prog->{code},
			);
			$interp->initialize;

			is $interp->current, 0, 'start at zero';
			is_deeply $interp->code, $prog->{code}, 'got code';
			is_deeply $interp->program, $prog->{code}, 'got mem';

			$interp->perform_replacement;
			$interp->run;

			is_deeply $interp->code, $prog->{code},
			    'still got code';

			is $interp->get_value($prog->{peek} // 0),
			    $prog->{value}, 'got stored value';
		}
	};
}
