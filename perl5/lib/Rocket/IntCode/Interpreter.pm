# Copyright (c) 2019  Peter Pentchev <roam@ringlet.net>
# All rights reserved.
#
# Redistribution and use in source and binary forms, with or without
# modification, are permitted provided that the following conditions
# are met:
# 1. Redistributions of source code must retain the above copyright
#    notice, this list of conditions and the following disclaimer.
# 2. Redistributions in binary form must reproduce the above copyright
#    notice, this list of conditions and the following disclaimer in the
#    documentation and/or other materials provided with the distribution.
#
# THIS SOFTWARE IS PROVIDED BY THE AUTHOR AND CONTRIBUTORS ``AS IS'' AND
# ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
# IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
# ARE DISCLAIMED.  IN NO EVENT SHALL THE AUTHOR OR CONTRIBUTORS BE LIABLE
# FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
# DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS
# OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
# HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
# LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY
# OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF
# SUCH DAMAGE.

use v5.012;
use strict;
use warnings;

package Rocket::IntCode::Interpreter;

use v5.012;
use strict;
use warnings;

use Moo;
use strictures 2;
use namespace::clean;

has replace => (
	is => 'ro',
);

has code => (
	is => 'ro',
);

has program => (
	is => 'rw',
);

has current => (
	is => 'rw',
);

use constant OP_ADD => 1;
use constant OP_MUL => 2;

use constant OP_STOP => 99;

sub initialize($)
{
	my ($self) = @_;

	$self->program([@{$self->code}]);
	$self->current(0);
}

sub get_value($ $)
{
	my ($self, $offset) = @_;

	return 0 if $offset >= scalar @{$self->program};
	return $self->program->[$offset];
}

sub set_value($ $ $)
{
	my ($self, $offset, $value) = @_;

	if ($offset >= scalar @{$self->program}) {
		push @{$self->program},
		    (0) x ($offset - scalar @{$self->program});
	}
	$self->program->[$offset] = $value;
}

sub perform_replacement($)
{
	my ($self) = @_;
	
	return unless defined $self->replace;
	my ($noun, $verb) = (int($self->replace / 100), $self->replace % 100);
	$self->set_value(1, $noun);
	$self->set_value(2, $verb);
}

my %arith_handlers = (
	OP_ADD() => sub { $_[0] + $_[1] },
	OP_MUL() => sub { $_[0] * $_[1] },
);

sub do_arith($ $)
{
	my ($self, $opcode) = @_;

	my ($offset_1, $offset_2, $store) =
	    @{$self->program}[$self->current + 1..$self->current + 3];
	my ($op1, $op2) =
	    ($self->get_value($offset_1), $self->get_value($offset_2));
	say "current ".$self->current." op $opcode ".
	    "ofs1 $offset_1 ofs2 $offset_2 store $store ".
	    "op1 $op1 op2 $op2";
	my $result = $arith_handlers{$opcode}->($op1, $op2);
	$self->set_value($store, $result);

	$self->current($self->current + 4);
}

my %op_handlers = (
	OP_ADD() => \&do_arith,
	OP_MUL() => \&do_arith,
);

sub run($)
{
	my ($self) = @_;

	while (1) {
		my $opcode = $self->program->[$self->current];
		return if $opcode == OP_STOP;

		my $handler = $op_handlers{$opcode};
		die "Invalid opcode $opcode\n" unless defined $handler;
		$handler->($self, $opcode);
	}
}

1;
