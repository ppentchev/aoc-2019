#!/usr/bin/env perl6
#
# Copyright (c) 2019  Peter Pentchev
# All rights reserved.
#
# Redistribution and use in source and binary forms, with or without
# modification, are permitted provided that the following conditions
# are met:
# 1. Redistributions of source code must retain the above copyright
#    notice, this list of conditions and the following disclaimer.
# 2. Redistributions in binary form must reproduce the above copyright
#    notice, this list of conditions and the following disclaimer in the
#    documentation and/or other materials provided with the distribution.
#
# THIS SOFTWARE IS PROVIDED BY THE AUTHOR AND CONTRIBUTORS ``AS IS'' AND
# ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
# IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
# ARE DISCLAIMED.  IN NO EVENT SHALL THE AUTHOR OR CONTRIBUTORS BE LIABLE
# FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
# DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS
# OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
# HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
# LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY
# OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF
# SUCH DAMAGE.

use v6;

use Getopt::Std;
use Shell::Capture;

constant VERSION-STRING = '0.1.0';

my Bool ($debug, $noop);

sub version()
{
	say 'rocket ' ~ VERSION-STRING;
}

sub usage(Bool:D $err = True)
{
	my $s = q:to/EOUSAGE/;
Usage:	rocket [-Nv] 01-1 weight-file.txt
	rocket [-Nv] 01-2 weight-file.txt
	rocket implemented
	rocket -V | -h

	-h	display program usage information and exit
	-V	display program version information and exit
EOUSAGE

	if $err {
		$*ERR.print($s);
		exit 1;
	} else {
		print $s;
	}
}

sub show-features()
{
	say 'Features: skeleton=' ~ VERSION-STRING;
}

sub note-fatal(Str:D $s)
{
	note $s;
	exit 1;
}

sub debug(Str:D $s)
{
	note $s if $debug;
}

sub help_or_version(Hash $opts)
{
	my Bool:D $has_dash = defined $opts<->;
	my Bool:D $dash_help = $has_dash && $opts<-> eq 'help';
	my Bool:D $dash_version = $has_dash && $opts<-> eq 'version';
	my Bool:D $dash_features = $has_dash && $opts<-> eq 'features';

	if $has_dash && !$dash_help && !$dash_version && !$dash_features {
		note "Invalid long option '$opts<->' specified";
		usage True;
	}
	version if $opts<V> || $dash_version;
	usage False if $opts<h> || $dash_help;
	show-features if $dash_features;
	exit(0) if $opts{<V h ->}:k;
}

sub calc-fuel(Int:D $value) returns Int:D
{
	return floor($value ÷ 3) - 2;
}

sub calc-fuel-recursive(Int:D $value) returns Int:D
{
	my Int:D $current = $value;
	my Int:D $total = 0;
	loop {
		my Int:D $fuel = calc-fuel $current;
		return $total if $fuel ≤ 0;
		$total += $fuel;
		$current = $fuel;
	}
}

sub calc-weight(Str:D $filename, Block:D $calc)
{
	my Int:D $total = 0;
	for $filename.IO.lines -> $line {
		my Int:D $value = Int($line);
		my Int:D $fuel = $calc($value);
		$total += $fuel;
		say "$value $fuel $total";
	}
	say $total;
}

sub calc-weight-naive(Str:D $cmd, Str:D @args)
{
	note-fatal 'Need a single filename argument' if @args.elems ≠ 1;
	calc-weight @args[0], &calc-fuel
}

sub calc-weight-recursive(Str:D $cmd, Str:D @args)
{
	note-fatal 'Need a single filename argument' if @args.elems ≠ 1;
	calc-weight @args[0], &calc-fuel-recursive
}

my %handlers = (
	'01-1' => &calc-weight-naive,
	'01-2' => &calc-weight-recursive,
	'implemented' => &show-implemented,
);

sub show-implemented(Str:D $cmd, Str:D @args)
{
	note-fatal "No arguments to 'implemented'" if @args;
	.say for %handlers.keys.grep(/^<[0..9]><[0..9]> '-' <[0..9]>$/).sort;
}

{
	my %opts = do {
		CATCH { when X::Getopt::Std { .message.note; usage; } };
		getopts('hV-:', @*ARGS)
	};
	help_or_version %opts;

	$debug = ?%opts<v>;

	usage unless @*ARGS;
	my (Str:D $cmd, Str:D @args) = @*ARGS;
	%handlers{$cmd}($cmd, @args);
}
