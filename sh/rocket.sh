#!/bin/sh
#
# Copyright (c) 2019  Peter Pentchev
# All rights reserved.
#
# Redistribution and use in source and binary forms, with or without
# modification, are permitted provided that the following conditions
# are met:
# 1. Redistributions of source code must retain the above copyright
#    notice, this list of conditions and the following disclaimer.
# 2. Redistributions in binary form must reproduce the above copyright
#    notice, this list of conditions and the following disclaimer in the
#    documentation and/or other materials provided with the distribution.
#
# THIS SOFTWARE IS PROVIDED BY THE AUTHOR AND CONTRIBUTORS ``AS IS'' AND
# ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
# IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
# ARE DISCLAIMED.  IN NO EVENT SHALL THE AUTHOR OR CONTRIBUTORS BE LIABLE
# FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
# DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS
# OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
# HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
# LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY
# OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF
# SUCH DAMAGE.

set -e

version_string='0.1.0'

version()
{
	echo "rocket $version_string"
}

features()
{
	echo "Features: rocket=$version_string"
}

usage()
{
	cat <<EOUSAGE
Usage:	rocket 01-1 weightfile.txt
	rocket 01-2 weightfile.txt
	rocket 02-1 programfile.txt
	rocket 02-2 programfile.txt
	rocket implemented
	rocket -V | -h | --version | --help 
	rocket --features

	-h	display program usage information and exit
	-V	display program version information and exit
EOUSAGE
}

calc_fuel()
{
	local weight="$1"
	local third="$((weight / 3))"
	if [ "$third" -lt 2 ]; then
		echo 0
	else
		printf -- '%d\n' "$((third - 2))"
	fi
}

calc_fuel_recursive()
{
	local weight="$1"
	local total=0

	while true; do
		fuel="$(calc_fuel "$weight")"
		if [ "$fuel" -eq 0 ]; then
			printf -- '%d\n' "$total"
			return
		fi

		total="$((total + fuel))"
		weight="$fuel"
	done
}

run_weight()
{
	if [ "$#" -ne 2 ]; then
		echo 'A single weight filename expected' 1>&2
		exit 1
	fi
	local mode="$1"
	local fname="$2"

	total=0
	while read value; do
		case "$mode" in
			naive)
				fuel="$(calc_fuel "$value")"
				;;

			recursive)
				fuel="$(calc_fuel_recursive "$value")"
				;;

			*)
				echo "Internal error: weight mode $mode" 1>&2
				exit 1
				;;
		esac

		total="$((total + fuel))"
		printf -- '%d %d %d\n' "$value" "$fuel" "$total"
	done < "$fname"
	printf -- '%d\n' "$total"
}

intcode_parse()
{
	local fname="$1"

	i_code="$tempd/code"
	mkdir -p -- "$i_code"
	i_code_len=0

	i_replace=-1
	i_peek=0
	i_seek=0

	local line=''
	while read line; do
		if [ "${line#*=}" != "$line" ]; then
			local var="${line%%=*}"
			local value="${line#*=}"
			case "$var" in
				replace)
					i_replace="$value"
					;;

				peek)
					i_peek="$value"
					;;

				seek)
					i_seek="$value"
					;;

				*)
					echo "Unrecognized variable $var" 1>&2
					;;
			esac
			continue
		fi

		while [ -n "$line" ]; do
			local first="${line%%,*}"
			local rest="${line#*,}"

			printf -- '%d\n' "$first" > "$i_code/$i_code_len"
			i_code_len="$((i_code_len + 1))"

			if [ "$line" = "$rest" ]; then
				break
			else
				line="$rest"
			fi
		done
	done < "$fname"
}

intcode_set_value()
{
	local offset="$1" value="$2"

	while [ "$offset" -ge "$i_memory_len" ]; do
		echo 0 > "$i_memory/$i_memory_len"
		i_code_len="$((i_memory_len + 1))"
	done

	printf -- '%d\n' "$value" > "$i_memory/$offset"
}

intcode_get_value()
{
	local offset="$1"

	if [ "$offset" -ge "$i_memory_len" ]; then
		echo 0
	else
		cat -- "$i_memory/$offset"
	fi
}

intcode_init()
{
	i_memory="$tempd/memory"
	rm -rf -- "$i_memory"
	mkdir -p -- "$i_memory"
	cp -- "$i_code/"* "$i_memory/"
	i_memory_len="$i_code_len"

	if [ "$i_replace" != '-1' ]; then
		local noun="$((i_replace / 100))"
		local verb="$((i_replace % 100))"
		intcode_set_value 1 "$noun" 
		intcode_set_value 2 "$verb"
	fi

	i_current=0
}

intcode_run()
{
	while true; do
		local opcode="$(intcode_get_value "$i_current")"
		if [ "$opcode" = 99 ]; then
			return
		fi

		local offset_1="$(intcode_get_value "$((i_current + 1))")"
		local offset_2="$(intcode_get_value "$((i_current + 2))")"
		local store="$(intcode_get_value "$((i_current + 3))")"

		local op1="$(intcode_get_value "$offset_1")"
		local op2="$(intcode_get_value "$offset_2")"

		case "$opcode" in
			1)
				result="$((op1 + op2))"
				;;

			2)
				result="$((op1 * op2))"
				;;

			*)
				echo "Invalid opcode $opcode at $i_current" 1>&2
				exit 1
				;;
		esac
		intcode_set_value "$store" "$result"

		i_current="$((i_current + 4))"
	done
}

run_intcode_run()
{
	intcode_init
	intcode_run

	local value="$(intcode_get_value "$i_peek")"
	printf -- '%d %d\n' "$i_peek" "$value"
}

run_intcode_seek()
{
	local noun='' verb=''
	for noun in $(seq 0 "$i_code_len"); do
		for verb in $(seq 0 "$i_code_len"); do
			i_replace="$((noun * 100 + verb))"
			intcode_init
			intcode_run
			local value="$(intcode_get_value "$i_peek")"
			echo "tried noun $noun verb $verb got $value/$i_seek"
			if [ "$value" = "$i_seek" ]; then
				printf -- '%d\n' "$i_replace"
				return
			fi
		done
	done
	printf -- '-1\n'
}

run_intcode()
{
	if [ "$#" -ne 2 ]; then
		echo 'A single program filename expected' 1>&2
		exit 1
	fi
	local mode="$1"
	local fname="$2"

	intcode_parse "$fname"

	case "$mode" in
		run)
			run_intcode_run
			;;

		seek)
			run_intcode_seek
			;;

		*)
			echo "Internal error: intcode mode '$mode'" 1>&2
			exit 1
			;;
	esac
}

unset hflag Vflag show_features

while getopts 'hNVv-:' o; do
	case "$o" in
		h)
			hflag=1
			;;

		V)
			Vflag=1
			;;

		-)
			if [ "$OPTARG" = 'help' ]; then
				hflag=1
			elif [ "$OPTARG" = 'version' ]; then
				Vflag=1
			elif [ "$OPTARG" = 'features' ]; then
				show_features=1
			else
				echo "Invalid long option '$OPTARG' specified" 1>&2
				usage 1>&2
				exit 1
			fi
			;;

		*)
			usage 1>&2
			exit 1
			;;
	esac
done
[ -z "$Vflag" ] || version
[ -z "$hflag" ] || usage
[ -z "$show_features" ] || features
[ -z "$Vflag$hflag$show_features" ] || exit 0

shift `expr "$OPTIND" - 1`
if [ "$#" -lt 1 ]; then
	usage 1>&2
	exit 1
fi

tempd=`mktemp -d -t rocket-data.txt.XXXXXX`
trap "rm -rf -- '$tempd'" EXIT QUIT TERM INT HUP 

cmd="$1"
shift

case "$cmd" in
	implemented)
		printf '01-1\n01-2\n02-1\n02-2\n'
		;;

	01-1)
		run_weight naive "$@"
		;;

	01-2)
		run_weight recursive "$@"
		;;

	02-1)
		run_intcode run "$@"
		;;

	02-2)
		run_intcode seek "$@"
		;;

	*)
		usage 1>&2
		exit 1
		;;
esac
