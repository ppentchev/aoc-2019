This is a collection of implementations of the tasks in
the 2019 edition of the Advent of Code in various
programming languages.

To test a single implementation, change to the `rocket-funk/`
subdirectory and run the `run-functional-tests.sh` shell
script, giving it the name and arguments of the program to
test, e.g.:

    ./run-functional-tests.sh perl6 ../raku/aoc.p6
    ./run-functional-tests.sh perl ../perl5/aoc.pl
    ./run-functional-tests.sh python3 -m rocket
    ./run-functional-tests.sh ../c/aoc

For an even easier time, use the `runfunc.py` tool also from
the `rocket-funk/` subdirectory:

    ./runfunc.py list
    ./runfunc.py matrix
    ./runfunc.py run

And for an even easier easier time, use the `funk.sh` tool also
from the, you guessed it, `rocket-funk` subdirectory:

    rocket-funk/funk.sh list
    rocket-funk/funk.sh matrix
    rocket-funk/funk.sh run

Note that due to, well, how do we put it, performance issues :)
the POSIX shell implementation (yes, there is a POSIX shell
implementation) shall not be fully tested (some of the most
time-consuming tests will be skipped) unless the `RUN_FULL_SHELL`
environment variable is set to a non-empty value when running
`run-functional-tests.sh`, `runfunc.py`, or `funk.sh`.
